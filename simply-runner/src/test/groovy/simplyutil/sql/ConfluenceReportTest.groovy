package simplyutil.sql;


import org.junit.Test
import groovy.sql.Sql
import org.apache.commons.io.FileUtils
import static org.junit.Assert.assertEquals
import org.junit.Before

/**
 * Created: 9/07/12
 *
 * @author david
 */
public class ConfluenceReportTest {
    File testDir

    @Before
    void setup() {
        testDir = new File("ConfluenceReportTest")
        FileUtils.deleteDirectory(testDir)
        testDir.mkdirs()
    }

    @Test
    public void testProcess() {
        File inputDir = new File(testDir, "sql")
        inputDir.mkdirs()
        File queryHeadingFile = new File(inputDir, "test1.prefix.txt")
        queryHeadingFile.text = """h3. Query with description
The following includes the description column"""
        File queryFile = new File(inputDir, "test1.sql")
        queryFile.text = "select * from test;"
        File queryHeadingFile2 = new File(inputDir, "test2.prefix.txt")
        queryHeadingFile2.text = """h3. Query without description
The following does not include the description column"""
        File queryFile2 = new File(inputDir, "test2.sql")
        queryFile2.text = 'select ${COLUMNS} from test;'

        File configFile = new File(getClass().getClassLoader().getResource('sql/input/config.groovy').toURI())
        Sql sql = ConfluenceReport.loadSqlConfig("test", configFile)
        createTestTable(sql)
        File outputFile = new File(inputDir.absolutePath, "report.txt")
        ConfluenceReport.main(["-e", "test", "-i", inputDir.absolutePath, "-o", outputFile, "-c", configFile.absolutePath, "-r", "COLUMNS=id,forename,surname"] as String[])
        assertEquals("""h3. Query with description
The following includes the description column
||ID||FORENAME||SURNAME||DESCRIPTION||
|1|David|Smith||
|2|Joe|Blogs|code monkey|
|3|Albert|Einstein|Scientist|
h3. Query without description
The following does not include the description column
||ID||FORENAME||SURNAME||
|1|David|Smith|
|2|Joe|Blogs|
|3|Albert|Einstein|
""", outputFile.text)
    }

    def createTestTable(Sql sql) {
        File testData = new File(getClass().getClassLoader().getResource('sql/input/database.csv').toURI())
        createTableFromCsv(sql, "test", testData)
    }

    public void createTableFromCsv(Sql sql, String tableName, File csv) {
        // FIX - when run on a new machine, need to avoid doing drop when table does not exist
        sql.execute("drop table IF EXISTS ${Sql.expand tableName};")
        sql.execute("create table ${Sql.expand tableName} as select * from CSVREAD('${Sql.expand csv.absolutePath}');")
    }

}
