package simplyutil.sql

import groovy.sql.Sql
import simplygroovy.sql.DefaultSqlToConfluence
import org.apache.commons.cli.Option

/**
 * Created: 9/07/12
 * @author david
 */
public class ConfluenceReport {
    public static void main(String[] args) {
        CliBuilder cli = new CliBuilder(usage: "groovy PrettyPrint.groovy -i input [-o outputFile -d outputDir]")
        cli.e(argName: 'env', longOpt: 'env', args: 1, required: true, 'environment to use from the credentials file')
        cli.i(argName: 'inputDir', longOpt: 'inputDir', args: 1, required: true, 'input dir containing queries to execute')
        cli.o(argName: 'outputFile', longOpt: 'outputFile', args: 1, required: true, 'output file to contain report')
        cli.c(argName: 'credentialsFile', longOpt: 'credentialsFile', args: 1, required: true, 'database settings')
        cli.r(argName: 'replace', longOpt: 'replace', args: Option.UNLIMITED_VALUES, valueSeparator: ';', required: false, 'query replacement tokens eg -r key=value')
        def options = cli.parse(args)
        if (!options) {
            println "complete"
            return
        }
        String env = options.e

        File credentialsFile = new File(options.c)
        File inputDir = new File(options.i)
        File outputFile = new File(options.o)
        Map<String, String> replacements = [:]
        if(options.rs) {
            replacements = parseReplacements(options.rs)
        }
        new ConfluenceReport().process(env, inputDir, outputFile, credentialsFile, replacements)
    }

    static Map<String, String> parseReplacements(List<String> keyValuePairs) {
        Map<String, String> replacements = [:]
        keyValuePairs.each { String pair ->
            String[] parts = pair.split("=")
            replacements[parts[0]] = parts[1]
        }
        return replacements
    }

    public void process(String env, File inputDir, File outputFile, File credentialsFile, Map<String, String> replacements = [:]) {
        Sql sql = loadSqlConfig(env, credentialsFile)
        outputFile.text = new DefaultSqlToConfluence().processFiles(inputDir, sql, replacements)
    }

    public static Sql loadSqlConfig(String env, File credentialsFile) {
        ConfigObject credentialsConfig = new ConfigSlurper(env).parse(credentialsFile.toURL())

        String sqlusername = credentialsConfig.sql.username
        String sqlpassword = credentialsConfig.sql.password
        String driver = credentialsConfig.sql.driver
        String connectionstring = credentialsConfig.sql.connectionstring

        Sql sql = Sql.newInstance(connectionstring, sqlusername, sqlpassword, driver)
        return sql
    }
}
