@echo off

rem setlocal

set SCRIPT_DIR=%~dp0
set CLASSPATH=%SCRIPT_DIR%\..\lib\*;
set CLASS_TO_RUN=%1

if X%CLASS_TO_RUN% == X goto usage

shift

java -Xmx1024M -classpath %CLASSPATH% "%CLASS_TO_RUN%" %1 %2 %3 %4 %5 %6 %7 %8 %9 

if errorlevel 1 exit /b 1
echo OK
goto end

:usage
echo Usage: 
echo       groovyRunner.cmd script-to-run [up to 9 optional arguments]
echo example usage: 
echo       groovyRunner.cmd myscript.groovy 1 2 3 4 5 6 7 8 9
goto end

:end
endlocal