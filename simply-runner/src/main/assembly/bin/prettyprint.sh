#!/bin/sh

scriptDir=`dirname $(cd "${0%/*}" 2>/dev/null; echo "$PWD"/"${0##*/}")`
echo script.dir=$scriptDir
java -classpath .:$scriptDir/../lib/* -Dscript.dir="${scriptDir}" simplyutil.prettyprint.PrettyPrinter $@