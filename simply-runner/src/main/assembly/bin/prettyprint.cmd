@echo off
echo starting...
set SCRIPT_DIR=%~dp0
echo script dir %SCRIPT_DIR%
java -classpath "%SCRIPT_DIR%..\lib\*" -Dscript.dir=%SCRIPT_DIR% simplyutil.prettyprint.PrettyPrinter %*
exit /B %ERRORLEVEL%
