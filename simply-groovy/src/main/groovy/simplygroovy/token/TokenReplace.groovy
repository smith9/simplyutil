package simplygroovy.token

public interface TokenReplace {
    String replaceAll(String input, Map replacements)
}