package simplygroovy.sql

import groovy.sql.Sql

/**
 * Created: 25/03/11
 * @author david
 */
public interface SqlToCsv {
    void toCsv(File csv, Sql runner, String query, Map<String, String> replacements)
}