package simplygroovy.sql

import groovy.sql.Sql
import simplygroovy.csv.CsvWriter
import simplygroovy.csv.opencsv.OpenCsvWriter
import simplygroovy.token.DefaultTokenReplace

/**
 * Created: 25/03/11
 * @author david
 */
class DefaultSqlToConfluence {
    private static final String NEWLINE = System.getProperty("line.separator")

    public String processFiles(File inputDir, Sql sql, Map<String, String> replacements) {
        StringBuilder builder = new StringBuilder()
        File headerText = new File(inputDir, "header.txt")
        if(headerText.exists()) {
            builder.append(headerText.text).append(NEWLINE)
        }
        List<File> sqlFiles = inputDir.listFiles(
                {dir, file-> file ==~ /.*?\.sql/ } as FilenameFilter
        ).sort {it.name}
        sqlFiles.each { File sqlFile ->
            File prefixText = new File(sqlFile.parentFile, sqlFile.name - ".sql" + ".prefix.txt")
            if(prefixText.exists()) {
                builder.append(prefixText.text).append(NEWLINE)
            }
            builder.append(new DefaultSqlToConfluence().toTable(sql, sqlFile.text, replacements))
            File suffixText = new File(sqlFile.parentFile, sqlFile.name - ".sql" + ".suffix.txt")
            if(suffixText.exists()) {
                builder.append(suffixText.text).append(NEWLINE)
            }
        }
        return builder.toString()
    }

    public String toTable(Sql runner, String query, Map<String, String> replacements) {
        String replacedQuery = new DefaultTokenReplace().replaceAll(query, replacements)
        StringBuilder builder = new StringBuilder()
        boolean firstRow = true
        runner.eachRow(replacedQuery) { row ->
            def rowResult = row.toRowResult()
            if(firstRow) {
                firstRow = false
                builder.append("||").append(rowResult.keySet().collect { it }.join("||")).append("||").append(NEWLINE)
            }
            builder.append("|").append(rowResult.values().collect { it == null ? "" : it}.join("|")).append("|").append(NEWLINE)
        }
        return builder.toString()
    }
}
