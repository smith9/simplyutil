package simplygroovy.sql


import groovy.sql.Sql
import simplygroovy.token.DefaultTokenReplace
import simplygroovy.csv.CsvWriter
import simplygroovy.csv.opencsv.OpenCsvWriter

/**
 * Created: 25/03/11
 * @author david
 */
class DefaultSqlToCsv implements SqlToCsv {

    public void toCsv(File csv, Sql runner, String query, Map<String, String> replacements) {
        String replacedQuery = new DefaultTokenReplace().replaceAll(query, replacements)
        if(!csv.getParentFile().exists()) {
            csv.getParentFile().mkdirs()
        }

        CsvWriter writer = new OpenCsvWriter(csv)
        boolean firstRow = true
        runner.eachRow(replacedQuery) { row ->
            def rowResult = row.toRowResult()
            if(firstRow) {
                firstRow = false
                writer.writeRow(rowResult.keySet().collect { it } as List<String>)
            }
            writer.writeRow(rowResult.values() as List<String>)
        }
        writer.close()
    }
}
