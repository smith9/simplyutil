package simplygroovy.sql.query

/**
 * Created: 19/06/11
 * @author david
 */
class Select implements QueryLine {
    Convert convert

    List<String> groupByEntries = []

    private List<Table> fromTables = []
    private List<Where> whereClauses = []
    private List<Join> joinClauses = []
    private List<String> orderByEntries = []
    private Columns columns
    private boolean endInSemiColon = true

    Select(Convert convert) {
        this.convert = convert
        columns = new Columns()
    }

    Where where(String field) {
        Where where = new Where(convert, field)
        return add(where)
    }

    Where orWhere(String field) {
        return createWhere(Logical.Or, field)
    }

    Where andWhere(String field) {
        return createWhere(Logical.And, field)
    }

    private Where createWhere(Logical logical, String field) {
        Where where = new Where(convert, logical, field)
        return add(where)
    }

    public Where add(Where where) {
        whereClauses.add(where)
        return where
    }

    public String toString() {
        return asString()
    }

    String asString(String separator = System.getProperty("line.separator")) {
        StringBuilder builder = new StringBuilder()
        buildColumns(builder, separator)
        buildFromTable(builder, separator)
        buildJoinClauses(builder, separator)
        buildWhereClauses(builder, separator)
        buildGroupByClause(builder, separator)
        buildOrderByClause(builder, separator)
        if(endInSemiColon) {
            builder.append(";")
        }
        return builder.toString()
    }

    void buildOrderByClause(StringBuilder builder, String separator) {
        if(!orderByEntries.isEmpty()) {
            builder.append(separator).append("ORDER BY ")
            builder.append(orderByEntries.join(", "))
        }
    }

    Table from(String fromTable, String fromTableAlias = null) {
        Table table = new Table(fromTable, fromTableAlias)
        table.convert = convert
        fromTables.add(table)
        return table
    }

    Table from(Table fromTable) {
        fromTable.convert = convert
        fromTables.add(fromTable)
        return fromTable
    }

    Select join(Join joinClause) {
        joinClauses.add(joinClause)
        return this
    }

    Select groupBy(String... groupByList) {
        groupByEntries.addAll(groupByList)
        return this
    }

    Select orderByDesc(String... orderBy) {
        orderByEntries.addAll(orderBy.collect { it + " DESC"})
        return this
    }

    Select orderBy(String... orderBy) {
        orderByEntries.addAll(orderBy)
        return this
    }

    Columns columns() {
        return columns
    }

    Columns columns(List<String> columnList) {
        columns.addAll(columnList)
        return columns
    }
    Select column(String columnName, String alias = null) {
        columns.column(columnName, alias)
        return this
    }

    Select column(Column column) {
        columns().addColumn(column)
        return this
    }
    Select withoutSemiColon() {
        endInSemiColon = false
        return this
    }
    Select clearGroupBy() {
        groupByEntries.clear()
    }

    Join join(String jointTable, String jointTableAlias = null) {
        Join join = new Join(jointTable, jointTableAlias).setConvert(convert)
        joinClauses.add(join)
        return join
    }

    @Override
    public Object clone() {
        Select cloned = new Select(convert)
        whereClauses.each { Where where ->
            cloned.add(where)
        }
        cloned.orderBy(orderByEntries as String[])
        cloned.groupBy(groupByEntries as String[])
        joinClauses.each { Join join ->
            cloned.join(join)
        }
        fromTables.each { Table fromTable ->
            cloned.from(fromTable)
        }
        if(!endInSemiColon) {
            cloned.withoutSemiColon()
        }
        columns.getColumnValues().each { Column column ->
            cloned.column(column)
        }
        return cloned
    }

    private void buildGroupByClause(StringBuilder builder, String separator) {
        if(!groupByEntries.isEmpty()) {
            builder.append(separator).append("GROUP BY ")
            builder.append(groupByEntries.join(", "))
        }
    }

    private void buildJoinClauses(StringBuilder builder, String separator) {
        joinClauses.each { Join join ->
            builder.append(separator).append(join.asString(separator))
        }
    }

    private def buildWhereClauses(def builder, String separator) {
        whereClauses.each { Where where ->
            builder.append(separator).append(where.asString(separator))
        }
    }

    private def buildFromTable(StringBuilder builder, String separator) {
        if (fromTables.isEmpty()) {
            throw new IllegalArgumentException("Must specify the from table in the SELECT, call from.")
        }
        List<String> tables = fromTables.collect { Table table -> table.asString(separator)}
        builder.append(separator).append("FROM ").append(tables.join("," + separator))
    }

    private def buildColumns(StringBuilder builder, String separator) {
        if (!columns) {
            throw new IllegalArgumentException("Must specify at least 1 column in the SELECT, call columns().")
        }
        builder.append("SELECT ").append(columns.asString(separator))
    }
}
