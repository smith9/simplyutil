package simplygroovy.sql.query

import simplygroovy.util.StringUtil

/**
 * Created: 17/06/11
 * @author david
 */
abstract class DefaultConvert implements Convert {
    Map<String, String> functions = [:]
    Map<String, String> values = [:]
    Map<String, String> dateFormat = new LinkedHashMap<String, String>()

    public void addFunction(String function, String dbFunction) {
        functions[function] = dbFunction
    }
    public void addValue(String value, String dbValue) {
        values[value] = dbValue
    }

    public void addDateFormat(String javaFormat, String dbFormat) {
        dateFormat[javaFormat] = dbFormat
    }

    @Override
    String function(String name) {
        return functions[name]
    }

    @Override
    String convertDateFormat(String javaFormat) {
        List<String> parts = new StringUtil().splitSame(javaFormat)
        List<String> transformed = parts.collect { String part ->
            if (dateFormat.containsKey(part)) {
                return dateFormat[part]
            }
            return part
        }

        return transformed.join()
    }

    @Override
    String value(String name) {
        return values[name]
    }

    @Override
    Select addRowNumber(String columnAsName, Select query) {
        String rowNumFunc = function(Convert.FUNC_ROW_NUMBER)
        if(rowNumFunc) {
            query.column("${rowNumFunc}()",columnAsName)
        } else {
            throw new UnsupportedOperationException("Row number not supported [call addFunction(Convert.FUNC_ROW_NUMBER, 'ROWNUM') or override Convert.addRowNumber]")
        }
    }
}
