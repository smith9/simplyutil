package simplygroovy.sql.query

/**
 * Created: 17/06/11
 * @author david
 */
class H2 extends DefaultConvert {

    H2() {
        addFunction(Convert.FUNC_TO_DATE, "FORMATDATETIME")
        addFunction(Convert.FUNC_TRUNC, "ROUND") //?
        addFunction(Convert.FUNC_ROW_NUMBER, "ROWNUM") //?
        addValue(VALUE_SYSDATE, "SYSDATE")
        addValue(VALUE_SET_ASSIGNMENT, "=")

        // Date format is same as java, so calls to addDateFormat
    }

    boolean fromTableIncludesSchema() {
        return false
    }


}
