package simplygroovy.sql.query

/**
 * Created: 19/06/11
 * @author david
 */
class Join implements QueryLine {
    private String tableName
    private String tableAlias
    private String onColumn1
    private String onColumn2
    private String schema
    private Convert convert

    Join(String tableName, String tableAlias = null) {
        this.tableName = tableName
        this.tableAlias = tableAlias
    }

    Join on(String column1, String column2) {
        onColumn1 = column1
        onColumn2 = column2
        return this
    }
    public Join setSchema(String schema) {
        this.schema = schema
        return this
    }
    public Join setConvert(Convert convert) {
        this.convert = convert
        return this
    }
    String asString(String separator) {
        StringBuilder builder = new StringBuilder()
        builder.append("JOIN ")
        if(convert.fromTableIncludesSchema()) {
            builder.append(schema).append(".")
        }
        builder.append(tableName)
        if(tableAlias) {
            builder.append(" AS ").append(tableAlias)
        }
        if(onColumn1) {
            builder.append(" ON ").append(onColumn1).append(" = ").append(onColumn2)
        }
        return builder.toString()
    }
}
