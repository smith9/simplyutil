package simplygroovy.sql.query

/**
 * Created: 19/06/11
 * @author david
 */
class Table implements QueryLine {
    private String schema
    private String name
    private String alias
    Convert convert

    Table(String name, String alias = null) {
        this.name = name
        this.alias = alias
    }

    public void setSchema(String schema) {
        this.schema = schema
    }

    String asString(String separator) {
        StringBuilder builder = new StringBuilder()
        if(schema && convert.fromTableIncludesSchema()) {
            builder.append(schema).append(".")
        }
        builder.append(name)
        if(alias) {
            builder.append(" AS ").append(alias)
        }
        return builder.toString()
    }
}
