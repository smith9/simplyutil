package simplygroovy.sql.query

import org.joda.time.DateTime

/**
 * Created: 17/06/11
 * @author david
 */
class Where implements QueryLine {
    Op operator
    Logical andOr
    Convert convert
    String field
    String dateFormat
    List<String> values = []

    public Where(Convert convert, String field) {
        this.convert = convert
        this.field = field
    }

    public Where(Convert convert, Logical andOr, String field) {
        this.convert = convert
        this.andOr = andOr
        this.field = field
    }

    @Override
    public Object clone() {
        Where cloned = new Where(convert, andOr, field)
        cloned.setOperator(operator)
        cloned.literalValue(values as String[])
        return cloned
    }

    public Where column(String columnRef) {
        return literalValue(columnRef)
    }

    public Where literalValue(String... literalValues) {
        values.addAll(literalValues)
        return this
    }

    public Where value(int ... intValues) {
        values.addAll(intValues)
        return this
    }

    public Where value(double ... doubleValues) {
        values.addAll(doubleValues)
        return this
    }

    public Where value(String... stringValues) {
        values.addAll(stringValues.collect { "'$it'" })
        return this
    }

    public Where date(String format, DateTime... dates) {
        return date(format, dates.collect {it.toString(format)} as String[])
    }

    public Where date(String format, String... dates) {
        String dbFormat = convert.convertDateFormat(format)
        dates.each { String date ->
            String toDateFunction = convert.function(Convert.FUNC_TO_DATE)
            values.add("$toDateFunction('$date', '$dbFormat')")
        }
        return this
    }

    public Where between() {
        return setOperator(Op.BETWEEN)
    }

    public Where equal() {
        return setOperator(Op.EQ)
    }

    public Where notEqual() {
        return setOperator(Op.NEQ)
    }

    public Where lessThan() {
        return setOperator(Op.LT)
    }

    public Where lessThanEqual() {
        return setOperator(Op.LTE)
    }

    public Where greaterThan() {
        return setOperator(Op.GT)
    }

    public Where greaterThanEqual() {
        return setOperator(Op.GTE)
    }

    public Where inList() {
        return setOperator(Op.IN)
    }

    public Where like(String value) {
        values.add("'$value'")
        return setOperator(Op.LIKE)
    }

    public Where isNull() {
        return setOperator(Op.IS_NULL)
    }

    public Where isNotNull() {
        return setOperator(Op.IS_NOT_NULL)
    }

    private Where setOperator(Op operator) {
        this.operator = operator
        return this
    }
    public String asString(String separator = System.getProperty("line.separator")) {
        if (!operator) {
            throw new IllegalArgumentException("No operator was specified. For example, equal() or isNull() was not called. ($field)")
        }
        StringBuilder builder = new StringBuilder()
        if (andOr) {
            builder.append(andOr.toString().toUpperCase())
        } else {
            builder.append("WHERE")
        }

        builder.append(" ").append(field)
        buildOperator(builder, separator)
        return builder.toString()
    }

    @Override
    public String toString() {
        return asString()
    }

    private def buildOperator(StringBuilder builder, String separator) {
        switch (operator) {
            case Op.BETWEEN:
                buildBetween(builder, separator)
                break
            case Op.IN:
                buildIn(builder)
                break
            case Op.EQ:
                buildSingleValueOp(builder, "=", "equal")
                break
            case Op.NEQ:
                buildSingleValueOp(builder, "<>", "not equal")
                break
            case Op.LT:
                buildSingleValueOp(builder, "<", "less than")
                break
            case Op.LTE:
                buildSingleValueOp(builder, "<=", "less than equal")
                break
            case Op.GT:
                buildSingleValueOp(builder, ">", "greater than")
                break
            case Op.GTE:
                buildSingleValueOp(builder, ">=", "greater than equal")
                break
            case Op.LIKE:
                buildSingleValueOp(builder, "LIKE", "LIKE")
                break
            case Op.IS_NULL:
                buildZeroValueOp(builder, "IS NULL")
                break
            case Op.IS_NOT_NULL:
                buildZeroValueOp(builder, "IS NOT NULL")
                break
            default:
                throw new UnsupportedOperationException("Operator is not supported. $operator ($field) ")
        }
    }

    private void buildIn(StringBuilder builder) {
        if (values.isEmpty()) {
            throw new IllegalArgumentException("Must specify at least 1 value for IN clause. ($field)")
        }
        String inList = values.join(', ')
        builder.append(" IN ($inList)")
    }

    private void buildZeroValueOp(StringBuilder builder, String op) {
        if (!values.isEmpty()) {
            throw new IllegalArgumentException("Can't specify values for $op clause. ($field)")
        }
        builder.append(" $op")
    }

    private void buildSingleValueOp(StringBuilder builder, String opSymbol, String opWord) {
        if (values.size() != 1) {
            throw new IllegalArgumentException("Can only specify 1 value for $opWord clause. ($field)")
        }
        builder.append(" $opSymbol ").append(values[0])
    }

    private void buildBetween(StringBuilder builder, String separator) {
        if (values.size() != 2) {
            throw new IllegalArgumentException("Can only specify 2 values for BETWEEN clause. ($field)")
        }
        builder.append(" BETWEEN ").append(values[0]).append(separator).append("AND ").append(values[1])
    }
}
