package simplygroovy.sql.query

/**
 * Created: 17/06/11
 * @author david
 */
class ConvertFactory {

    public ConvertFactory() {
    }

    public Convert create(String dbType) {
        switch(dbType) {
            case "MySql":
                return new MySql()
            case "H2":
                return new H2()
            default:
                throw new UnsupportedOperationException("Datebase is not supported or correct string not passed: $dbType")
        }
    }
}
