package simplygroovy.sql.query

/**
 * Created: 17/06/11
 * @author david
 */
class MySql extends DefaultConvert {

    MySql() {
        addFunction(Convert.FUNC_TO_DATE, "STR_TO_DATE")
        addFunction(Convert.FUNC_TRUNC, "TRUNC")
        addValue(VALUE_SYSDATE, "SYSDATE()")
        addValue(VALUE_SET_ASSIGNMENT, ":=")

        // Year
        addDateFormat("yyyy", "%Y")  // 4 digit year
        addDateFormat("yy", "%y")    // 2 digit year
        // Month
        addDateFormat("MMMMM", "%M") // January..December
        addDateFormat("MMMM", "%b")  // Jan..Dec
        addDateFormat("MM", "%m")    // 01-12 (actually 00-12 in MySql)
        addDateFormat("M", "%c")     // 1-12 (actually 0-12 in MySql)
        // Day
        addDateFormat("dd", "%d") // 01-31 (actually 00-31 in MySql)
        addDateFormat("d", "%e")  // 1-31  (actually 0-31 in MySql)
        // Week Day
        addDateFormat("EEEE", "%W") // Sunday..Saturday
        addDateFormat("EEE", "%a") // Sun..Sat
        // Hour
        addDateFormat("HH", "%H") // 00-23
        addDateFormat("H", "%k")  // 0-23
        addDateFormat("hh", "%h") // 01-12
        addDateFormat("h", "%l")  // 1-12
        // Minute
        addDateFormat("mm", "%i") // 00-59
        // Second
        addDateFormat("ss", "%s") // 00-59
        // Milli/Fractional
        addDateFormat("SSSSSS", "%f") // Java 000-999, MySQL 000000..999999
        // am/pm
        addDateFormat("a", "%p") // AM/PM
    }

    @Override
    Select addRowNumber(String columnAsName, Select query) {
        query.column("@rank:=@rank+1", columnAsName)
        query.from("(select @rank:=0)", "init${columnAsName}ToZero")
        query
    }

    boolean fromTableIncludesSchema() {
        return true
    }


}
