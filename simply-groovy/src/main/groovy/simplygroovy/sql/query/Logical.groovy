package simplygroovy.sql.query

/**
 * Created: 17/06/11
 * @author david
 */
enum Logical {
    And, Or
}
