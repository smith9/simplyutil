package simplygroovy.sql.query

/**
 * Created: 21/06/11
 * @author david
 */
class Column {
    String columnName
    String asName
    Aggregate aggregate
    public String dbResultName() {
        String name = columnName
        if(asName) {
            name = asName
        }
        return name//.toUpperCase()
    }
}
