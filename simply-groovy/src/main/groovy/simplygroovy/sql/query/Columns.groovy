package simplygroovy.sql.query

/**
 * Created: 19/06/11
 * @author david
 */
class Columns implements QueryLine {
    Map<String, Column> columnList = [:]

    public Columns(String... columns) {
        addAll(columns as List<String>)
    }

    public Columns() {
    }

    public Columns addColumn(Column column) {
        columnList[column.columnName] = column
        return this
    }

    public Columns addAll(List<String> columnsList) {
        columnList.putAll(columnsList.collectEntries { String columnName ->
            [columnName, new Column(columnName: columnName)]
        })
        return this
    }

    public Columns column(String columnName, String asName = null) {
        columnList[columnName] = new Column(columnName: columnName, asName: asName)
        return this
    }

    public Columns max(String columnToAggregate, String columnAs = null) {
        return aggregate(Aggregate.MAX, columnToAggregate, columnAs)
    }

    public Columns min(String columnToAggregate, String columnAs = null) {
        return aggregate(Aggregate.MIN, columnToAggregate, columnAs)
    }

    public Columns avg(String columnToAggregate, String columnAs = null) {
        return aggregate(Aggregate.AVG, columnToAggregate, columnAs)
    }

    public Columns count(String columnToAggregate, String columnAs = null) {
        return aggregate(Aggregate.COUNT, columnToAggregate, columnAs)
    }

    public Columns stdDevPop(String columnToAggregate, String columnAs = null) {
        return aggregate(Aggregate.STDDEV_POP, columnToAggregate, columnAs)
    }

    public Columns stdDevSample(String columnToAggregate, String columnAs = null) {
        return aggregate(Aggregate.STDDEV_SAMP, columnToAggregate, columnAs)
    }

    public Columns sum(String columnToAggregate, String columnAs = null) {
        return aggregate(Aggregate.SUM, columnToAggregate, columnAs)
    }

    private Columns aggregate(Aggregate aggregate, String columnToAggregate, String asName) {
        String columnName = "$aggregate($columnToAggregate)"
        columnList[columnName] = new Column(columnName: columnName, asName: asName, aggregate: aggregate)
        return this
    }

    public String asString(String separator) {
        if (columnList.isEmpty()) {
            throw new IllegalArgumentException("Must specify at least 1 column.")
        }
        List<String> columnsAs = columnList.collect { String columnName, Column column ->
            if (column.asName) {
                return "${column.columnName} AS ${column.asName}"
            }
            return "${column.columnName}"
        }
        return columnsAs.join("," + separator)
    }

    List<Column> getColumnValues() {
        return columnList.values() as List<Column>
    }

    List<Column> getAggregateColumns() {
        List<Column> aggregates = []
        columnList.each { String columnName, Column column ->
            if (column.aggregate) {
                aggregates.add(column)
            }
        }
        return aggregates
    }
    List<Column> getNonAggregateColumns() {
        List<Column> nonAggregates = []
        columnList.each { String columnName, Column column ->
            if (!column.aggregate) {
                nonAggregates.add(column)
            }
        }
        return nonAggregates
    }
    Column findAggregate(String measure, Aggregate aggregate) {
        return columnValues.find { Column column ->
            if(column.aggregate == aggregate && column.columnName.contains("($measure)")) {
                return true
            }
            return false
        }
    }
    Columns removeAggregates() {
        getAggregateColumns().each { Column column ->
            columnList.remove(column.columnName)
        }
        return this
    }

    @Override
    public Object clone() {
        Columns cloned = new Columns()
        columnList.each { String columnName, Column column ->
            cloned.addColumn(new Column(columnName: column.columnName, asName: column.asName, aggregate: column.aggregate))
        }

        return cloned
    }
}
