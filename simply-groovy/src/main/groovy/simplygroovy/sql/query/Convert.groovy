package simplygroovy.sql.query

/**
 * Created: 17/06/11
 * @author david
 */
public interface Convert {
    public static String FUNC_TO_DATE ="toDate"
    public static String FUNC_ROW_NUMBER ="rowNumber"
    public static String FUNC_TRUNC ="trunc"
    public static String VALUE_SYSDATE ="sysDate"
    public static String VALUE_SET_ASSIGNMENT ="set"

    String function(String name)
    String value(String name)
    String convertDateFormat(String javaFormat)

    public Select addRowNumber(String columnAsName, Select query)

    boolean fromTableIncludesSchema()
}