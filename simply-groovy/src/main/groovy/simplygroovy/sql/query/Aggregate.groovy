package simplygroovy.sql.query

/**
 * Created: 20/06/11
 * @author david
 */
public enum Aggregate {
    COUNT,
    MIN,
    MAX,
    SUM,
    AVG,
    STDDEV_POP,
    STDDEV_SAMP
}
