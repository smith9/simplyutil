package simplygroovy.sql.query

/**
 * Created: 18/06/11
 * @author david
 */
public enum Op {
    EQ, NEQ, GT, GTE, LT, LTE, IS_NULL, IS_NOT_NULL, LIKE, BETWEEN, IN
}