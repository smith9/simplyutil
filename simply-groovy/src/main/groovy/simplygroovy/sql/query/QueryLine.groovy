package simplygroovy.sql.query

/**
 * Created: 18/06/11
 * @author david
 */
public interface QueryLine {
    public String asString(String separator)
}