package simplygroovy.sql.stats

import simplygroovy.sql.query.Select

import simplygroovy.sql.query.Where
import simplygroovy.sql.query.Column
import groovy.sql.GroovyRowResult

/**
 * Created: 20/06/11
 * @author david
 */
class AggregateQuery {
    private Select nonPercentileQuery

    public Select createQuery(Select query, String percentileColumn, List<Integer> percentileRows) {
        nonPercentileQuery = query.clone()
        String orderedQuery = createOrdereredQuery(percentileColumn, nonPercentileQuery)
        String rowNumberQuery = createRowNumberQuery(orderedQuery, nonPercentileQuery)
        Select pickRowQuery = createPickRowQuery(rowNumberQuery, percentileRows, nonPercentileQuery)
        return pickRowQuery
    }

    String createOrdereredQuery(String percentileColumn, Select nonPercentileQuery) {
        List<String> originalGroupByColumns = new ArrayList(nonPercentileQuery.groupByEntries)
        originalGroupByColumns.add(percentileColumn)
        nonPercentileQuery.with {
            column(percentileColumn)
            withoutSemiColon()
            clearGroupBy()
            columns().removeAggregates()
            orderBy(originalGroupByColumns as String[])
        }
        return nonPercentileQuery.asString()
    }

    String createRowNumberQuery(String orderedQuery, Select nonPercentileQuery) {
        Select rowQuery = new Select(nonPercentileQuery.convert)
        // TODO mak "rank" a variable
        rowQuery.convert.addRowNumber("rank", rowQuery)
        rowQuery.with {
            column("OrderQuery.*")
            from("(${orderedQuery})", "OrderQuery")
            withoutSemiColon()
        }
        return rowQuery.asString()
    }

    Select createPickRowQuery(String rankedQuery, List<Integer> percentileRows, Select nonPercentileQuery) {
        Select pickQuery = new Select(nonPercentileQuery.convert)
        pickQuery.with {
            column("RankQuery.*")
            from("(${rankedQuery})", "RankQuery")
        }

        percentileRows.eachWithIndex { Integer row, int index->
            Where where
            if (index == 0) {
                // TODO seems to be a bug in H2 that doesn't allow 'in' clause with rownum
                where = pickQuery.where("rank")
            } else {
                where = pickQuery.orWhere("rank")
            }
            where.between().value(row, row)
        }
        return pickQuery
    }
}
