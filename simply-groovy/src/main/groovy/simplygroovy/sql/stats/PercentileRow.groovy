package simplygroovy.sql.stats

/**
 * Created: 24/06/11
 * @author david
 */
class PercentileRow {
    int lower
    int upper
    Double fractional
}
