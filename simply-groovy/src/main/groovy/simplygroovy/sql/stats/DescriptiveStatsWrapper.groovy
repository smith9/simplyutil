package simplygroovy.sql.stats

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

/**
 * Created: 8/06/12
 * @author david
 */
class DescriptiveStatsWrapper implements Statistics {
    List<Integer> percentileKeys = (1..100).collect { it }

    private Map<String, Object> columns = [:]
    DescriptiveStatistics stats

    DescriptiveStatsWrapper(DescriptiveStatistics stats) {
        this.stats = stats
    }

    void setAverage(Double average) {
        throw new UnsupportedOperationException("setAverage not supported")
    }

    Double getAverage() {
        return stats.mean
    }

    void setMin(Double min) {
        throw new UnsupportedOperationException("setMin not supported")
    }

    Double getMin() {
        return stats.min
    }

    void setMax(Double max) {
        throw new UnsupportedOperationException("setMax not supported")
    }

    Double getMax() {
        return stats.max
    }

    void setStdDevPop(Double stdDevPop) {
        throw new UnsupportedOperationException("setStdDevPop not supported")
    }

    Double getStdDevPop() {
        return Math.sqrt(stats.populationVariance)
    }

    void setStdDevSample(Double stdDevSample) {
        throw new UnsupportedOperationException("setStdDevSample not supported")
    }

    Double getStdDevSample() {
        return stats.standardDeviation
    }

    void setSum(Double sum) {
        throw new UnsupportedOperationException("setSum not supported")
    }

    Double getSum() {
        return stats.sum
    }

    void setCount(Integer count) {
        throw new UnsupportedOperationException("setCount not supported")
    }

    Integer getCount() {
        return stats.n
    }

    Statistics add(String column, Object value) {
        columns[column] = value
        return this
    }

    Object get(String column) {
        return columns[column]
    }

    Set<String> columnNames() {
        return columns.keySet()
    }

    Double getPercentile(int percentile) {
        return stats.getPercentile(percentile)
    }

    void setPercentile(int percentile, Double value) {
        throw new UnsupportedOperationException("setPercentile not supported")
    }

    Set<Integer> percentileKeys() {
        return percentileKeys
    }

    String joinColumnValues(String separator = ":") {
        return columns.values().join(separator)
    }
}
