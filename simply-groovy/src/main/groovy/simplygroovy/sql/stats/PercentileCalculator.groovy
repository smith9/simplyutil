package simplygroovy.sql.stats

import groovy.sql.GroovyRowResult

/**
 * Created: 23/06/11
 * @author david
 */
class PercentileCalculator {
    private List<AggregateStats> stats
    private List<Double> percentiles
    private Map<AggregateStats, AggregatePercentiles> percentileRows = [:]
    public Closure INTERPOLATED_PERCENTILES = { int numRows, double percentile ->
        PercentileRow row
        if (numRows > 0) {
            row = new PercentileRow()
            float estimatedPosition = percentile * (numRows + 1) / 100
            if (numRows == 1 || estimatedPosition < 1) {
                row.lower = 1
                row.upper = 1
            } else {
                if (estimatedPosition >= numRows) {
                    row.lower = numRows
                    row.upper = numRows
                } else {
                    double lowerPositionDouble = Math.floor(estimatedPosition)
                    row.lower = lowerPositionDouble as int
                    row.upper = row.lower
                    row.fractional = estimatedPosition - row.lower
                    if(row.fractional > 0.00000001) {
                        row.upper = row.lower + 1
                    }
                }
            }
        }
        return row
    }
    Closure percentileFunction = INTERPOLATED_PERCENTILES

    public PercentileCalculator(List<AggregateStats> stats, List<Double> percentiles) {
        this.stats = stats
        this.percentiles = percentiles
        initPercentileRows()
    }

    public List<Integer> calcPercentileRows() {
        List<Integer> rows = []
        percentileRows.values().each { AggregatePercentiles statRows ->
            statRows.values().each { PercentileRow percentileRow ->
                rows.add(percentileRow.lower)
                if (percentileRow.upper != percentileRow.lower) {
                    rows.add(percentileRow.upper)
                }
            }
        }
        return rows
    }

    public void addPercentileStats(Map<Integer, Double> rankValues) {
        stats.each { AggregateStats stat ->
            AggregatePercentiles aggregatePercentileRows = percentileRows[stat]
            aggregatePercentileRows.percentileRows.each { int percentile, PercentileRow percentileRow ->
                Double percentileValue = 0
                Double lowerValue = rankValues.get(percentileRow.lower, Double.NaN)
                if (percentileRow.lower == percentileRow.upper) {
                    percentileValue = lowerValue
                } else {
                    Double upperValue = rankValues.get(percentileRow.upper, Double.NaN)
                    percentileValue = lowerValue + percentileRow.fractional * (upperValue - lowerValue)
                }
                stat.setPercentile(percentile, percentileValue)
            }
        }
    }

    private def initPercentileRows() {
        int offset = 0
        stats.each { AggregateStats stat ->
            AggregatePercentiles statsRow = new AggregatePercentiles()
            percentileRows[stat] = statsRow
            if (stat.count > 0) {
                initPercentileRowsForAggregate(percentiles, stat.count, offset, statsRow)
            }
            offset += stat.count
        }
    }

    private def initPercentileRowsForAggregate(List<Integer> percentiles, int count, int offset, AggregatePercentiles statsRow) {
        percentiles.each { int percentile ->
            PercentileRow percentileRow = percentileFunction(count, percentile)
            percentileRow.lower += offset
            percentileRow.upper += offset
            statsRow.addPercentile(percentile, percentileRow)
        }
    }
}
