package simplygroovy.sql.stats

/**
 * Created: 20/06/11
 * @author david
 */
public class AggregateStats implements Statistics {
    private Map<String, Object> columns = [:]
    private Map<Integer, Double> percentiles = [:]
    int precisionOfPercentileKey = 0
    Double average
    Double min
    Double max
    Double stdDevPop
    Double stdDevSample
    Double sum
    Integer count

    public AggregateStats add(String column, Object value) {
        columns[column] = value
        return this
    }

    public Object get(String column) {
        return columns[column]
    }

    public Set<String> columnNames() {
        return columns.keySet()
    }

    public Double getPercentile(int percentile) {
        return percentiles[percentile]
    }

    public void setPercentile(int percentile, Double value) {
        percentiles[percentile] = value
    }

    public Set<Integer> percentileKeys() {
        return percentiles.keySet()
    }

//    public Iterator<Map.Entry<Integer, Double>> percentileIterator() {
//        return percentiles.entrySet().iterator()
//    }
//
//    public Iterator<Map.Entry<String, Object>> columnIterator() {
//        return columns.entrySet().iterator()
//    }

    public String toString ( ) {
    return "AggregateStats{" +
    "columns=" + columns +
    '}' ;
    }}
