package simplygroovy.sql.stats

import simplygroovy.sql.query.Select
import groovy.sql.Sql

import groovy.sql.GroovyRowResult
import simplygroovy.sql.query.Aggregate
import simplygroovy.sql.query.Column

/**
 * Created: 23/06/11
 * @author david
 */
class AggregateQueryRunner {
    Select query
    Sql sql

    public AggregateQueryRunner(Select query, Sql sql) {
        this.query = query
        this.sql = sql
    }

    public List<Statistics> calculateAggregates() {
        String queryString = query.asString()
        List<Statistics> stats = []
        sql.eachRow(queryString) { row ->
            def rowResult = row.toRowResult()
            AggregateStats stat = new AggregateStats()
            // todo pull out of loop
            query.columns().getAggregateColumns().each { Column column ->
                def value = rowResult.get(column.dbResultName())
                setAggregate(column, value, stat)
            }
            // todo pull out of loop
            query.columns().getNonAggregateColumns().each { Column column ->
                def value = rowResult.get(column.dbResultName())
                stat.add(column.dbResultName(), value)
            }
            stats.add(stat)
        }
        return stats
    }

    private void setAggregate(Column column, value, AggregateStats stat) {
        switch (column.aggregate) {
            case Aggregate.AVG:
                stat.average = value
                break
            case Aggregate.MIN:
                stat.min = value
                break
            case Aggregate.MAX:
                stat.max = value
                break
            case Aggregate.SUM:
                stat.sum = value
                break
            case Aggregate.COUNT:
                stat.count = value
                break
            case Aggregate.STDDEV_POP:
                stat.stdDevPop = value
                break
            case Aggregate.STDDEV_SAMP:
                stat.stdDevSample = value
                break
            default:
                throw new UnsupportedOperationException("Aggregate function ${column.aggregate} is not supported.")
                break
        }
    }

    public List<Statistics> calculatePercentiles(String percentileColumn, List<Integer> percentiles) {
        List<Statistics> stats = calculateAggregates()
        if (!stats.isEmpty()) {
            AggregateQuery aggregateQuery = new AggregateQuery()
            PercentileCalculator calc = new PercentileCalculator(stats, percentiles)
            List<Integer> percentileRows = calc.calcPercentileRows()
            String queryString = aggregateQuery.createQuery(query, percentileColumn, percentileRows).asString()
            List<GroovyRowResult> rows = sql.rows(queryString)
            String rankColumn = "rank"
            Map<Integer, Double> rankValues = [:]
            rows.each {
                rankValues.put(it.get(rankColumn) as Integer, it.get(percentileColumn))
            }

            calc.addPercentileStats(rankValues)
        }
        return stats
    }

}
