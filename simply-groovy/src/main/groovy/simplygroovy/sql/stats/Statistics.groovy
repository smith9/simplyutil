package simplygroovy.sql.stats

/**
 * Created: 8/06/12
 * @author david
 */
public interface Statistics {
    void setAverage(Double average)
    Double getAverage()

    void setMin(Double min)
    Double getMin()

    void setMax(Double max)
    Double getMax()

    void setStdDevPop(Double stdDevPop)
    Double getStdDevPop()

    void setStdDevSample(Double stdDevSample)
    Double getStdDevSample()

    void setSum(Double sum)
    Double getSum()

    void setCount(Integer count)
    Integer getCount()

    public Statistics add(String column, Object value)
    public Object get(String column)

    public Set<String> columnNames()

    public Double getPercentile(int percentile)

    public void setPercentile(int percentile, Double value)

    public Set<Integer> percentileKeys()
}