package simplygroovy.sql.stats

import simplygroovy.sql.query.Select
import groovy.sql.Sql
import simplygroovy.sql.query.Column
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

/**
 * Created: 8/06/12
 * @author david
 */
class PercentileQueryRunner {
    private Select query
    private Sql sql
    private String percentileColumn

    PercentileQueryRunner(Select query, Sql sql, String percentileColumn) {
        this.query = query
        this.sql = sql
        this.percentileColumn = percentileColumn
    }

    public List<Statistics> calculate() {
        String queryString = query.asString()
        List<String> columns = getKeyColumns()
        Map<String, DescriptiveStatsWrapper> stats = [:]
        sql.eachRow(queryString) { row ->
            double percentileColumnValue = row.getAt(percentileColumn)
            String lookupKey = joinColumns(row, columns)
            DescriptiveStatsWrapper stat = stats[lookupKey]
            if(!stat) {
                stat = createStat(row, keyColumns)
                stats[lookupKey] = stat
            }
            stat.stats.addValue(percentileColumnValue)
        }
        return sort(stats)
    }

    private List<Statistics> sort(Map<String, DescriptiveStatsWrapper> stats) {
        List<Statistics> sortedStats = stats.values() as List<Statistics>
        sortedStats.sort { it.joinColumnValues() }
        return sortedStats
    }

    private List<String> getKeyColumns() {
        List<String> keyColumns = []
        query.columns().nonAggregateColumns.each { Column column ->
            String columnName = column.dbResultName()
            if(!columnName.equalsIgnoreCase(percentileColumn)) {
                keyColumns << columnName
            }
        }
        return keyColumns
    }

    DescriptiveStatsWrapper createStat(def row, List<String> columns) {
        DescriptiveStatsWrapper stats = new DescriptiveStatsWrapper(new DescriptiveStatistics())
        columns.each { String columnName ->
            stats.add(columnName, row.getAt(columnName))
        }
        return stats
    }

    String joinColumns(def row, List<String> columns, String separator = ":") {
        StringBuilder builder = new StringBuilder()
        columns.each { String column ->
            builder.append(row.getAt(column)).append(separator)
        }
        return builder.toString()
    }
}
