package simplygroovy.sql.stats

/**
 * Created: 24/06/11
 * @author david
 */
class AggregatePercentiles {
    Map<Integer, PercentileRow> percentileRows = [:]

    void addPercentile(int percentile, PercentileRow percentileRow) {
        percentileRows[percentile] = percentileRow
    }

    List<PercentileRow> values() {
        return percentileRows.values() as List<PercentileRow>
    }
}
