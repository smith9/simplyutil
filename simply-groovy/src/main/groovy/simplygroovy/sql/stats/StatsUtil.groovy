package simplygroovy.sql.stats

/**
 * Created: 27/01/12
 * @author david
 */
class StatsUtil {
    public static Map<String, Set<Object>> collectColumnValues(List<Statistics> stats) {
        Map<String, Set<Object>> columnValues = [:]
        stats.each { Statistics stat ->
            stat.columnNames().each { String columnName ->
                Set<Object> values = columnValues[columnName]
                if(!values) {
                    values = []
                    columnValues[columnName] = values
                }
                Object value = stat.get(columnName)
                if(!values.contains(value)) {
                    values << value
                }
            }
        }
        return columnValues
    }
}
