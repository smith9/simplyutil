package simplygroovy.bean

/**
 * Created: 23/03/12
 * @author david
 */
public interface KVP {
    Object getKey()
    Object getValue()
}