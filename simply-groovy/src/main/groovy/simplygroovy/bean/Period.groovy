package simplygroovy.bean

/**
 * Created: 23/03/12
 * @author david
 */
public enum Period implements KVP {
    MINUTE('Minute', 0),
    HOUR('Hour', 1),
    DAY('Day', 2),
    MONTH('Month', 3),
    YEAR('Year', 4)
    int index
    Period(String value, int index) {
        this.value = value
        this.index = index
    }

    final String value

    Object getValue() {
        return value
    }
    Object getKey() {
        return name()
    }

    @Override
    String toString() {
        return getKey()
    }

    boolean isGreater(Period period) {
        return period.index < index
    }
}