package simplygroovy.bean

import org.apache.commons.beanutils.Converter

/**
 * Created: 27/04/12
 * @author david
 */
class EnumConverter implements Converter {
    public Object convert(Class type, Object value) {
        return Enum.valueOf(type, (String) value);
    }
}
