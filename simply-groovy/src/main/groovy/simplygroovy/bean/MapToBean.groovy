package simplygroovy.bean

import org.apache.commons.beanutils.ConvertUtils
import org.apache.commons.beanutils.BeanUtils

import org.apache.commons.beanutils.converters.DateConverter

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import static org.apache.commons.beanutils.ConvertUtils.*
import org.joda.time.format.ISODateTimeFormat

/**
 * Created: Mar 16, 2011
 * @author david
 */
class MapToBean {
    public static void registerEnumClass(Class enumClass) {
        if (!Enum.isAssignableFrom(enumClass)) {
            throw new RuntimeException("need to pass an Enum derived class")
        }
        register(new EnumConverter(), enumClass)
    }

    public static void setJodaDateTimeFormat(String format) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(format)
        registerJodaConverter(formatter)
    }
    public static void setJodaDateTimeFormatISO() {
        DateTimeFormatter formatter = ISODateTimeFormat.dateTime()
        registerJodaConverter(formatter)
    }

    private static def registerJodaConverter(DateTimeFormatter formatter) {
        DateTimeConverter dateConverter = new DateTimeConverter(formatter)
        register(dateConverter, DateTime.class)
    }

    public static void setDateFormat(String format) {
        org.apache.commons.beanutils.converters.DateConverter dateConverter = new org.apache.commons.beanutils.converters.DateConverter()
        dateConverter.setPattern(format)
        register(dateConverter, Date.class)
    }

    public static Object convert(Class beanType, Map properties) {
        Map filtered = properties.findAll { it.value != null}
        Object bean = beanType.newInstance()
        org.apache.commons.beanutils.BeanUtils.populate(bean, filtered)
        return bean
    }
}
