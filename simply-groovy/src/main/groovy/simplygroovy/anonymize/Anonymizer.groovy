package simplygroovy.anonymize

/**
 * Created: 23/05/11
 * @author david
 */
public interface Anonymizer {
    public String anonymize(String path, String element, String realValue)
    public String anonymize(String path, String element, String attribute, String realValue)
    public void setChoices(String path, List<String> choices)
    public void setOptions(String path, Map<String,String> options)
}