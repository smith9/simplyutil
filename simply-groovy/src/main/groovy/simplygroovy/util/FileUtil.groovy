package simplygroovy.util

import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import java.util.zip.ZipInputStream

/**
 * User: david
 * Date: 16/11/12
 * Time: 9:38 PM
 */
public class FileUtil {
    public static File zipFile(File dataFile, boolean deleteOriginal = false) {
        File zipFile = new File(dataFile.parentFile, dataFile.name + ".zip")
        ZipOutputStream zipOutStream = new ZipOutputStream(new FileOutputStream(zipFile))
        zipOutStream.putNextEntry(new ZipEntry(dataFile.name))
        FileInputStream dataFileStream = new FileInputStream(dataFile)
        zipOutStream << dataFileStream
        zipOutStream.closeEntry()
        zipOutStream.close()
        dataFileStream.close()
        if (deleteOriginal) {
            org.apache.commons.io.FileUtils.forceDelete(dataFile)
        }
        return zipFile
    }

    static List<File> unzip(File zipFile, File outputDir) {
        ZipInputStream zipInStream = new ZipInputStream(new FileInputStream(zipFile))
        List<File> files = []
        ZipEntry entry = null
        while ((entry = zipInStream.getNextEntry()) != null) {
            System.out.println("Unzipping " + entry.getName())
            File outputFile = new File(outputDir, entry.getName())
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile)
            fileOutputStream << zipInStream
            zipInStream.closeEntry()
            fileOutputStream.close()
            files << outputFile
        }
        zipInStream.close();

        return files

    }

}
