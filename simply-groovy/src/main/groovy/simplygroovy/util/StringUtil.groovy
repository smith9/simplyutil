package simplygroovy.util

/**
 * Created: 18/06/11
 * @author david
 */
class StringUtil {
    /**
     * Splits a string into parts where each part contains the same characters
     * For example "AAB:C" is split into "AA", "B", ":", "C"
     * @param input
     * @return
     */

    public List<String> splitSame(String input){
        List<String> parts = []
        Character lastChar = null
        StringBuilder currentPart = new StringBuilder()
        input.chars.each { char currChar ->
            if (currChar == lastChar || !lastChar) {
                currentPart.append(currChar)
            } else {
                parts.add(currentPart.toString())
                currentPart = new StringBuilder(currChar as String)
            }

            lastChar = currChar
        }
        if(currentPart.toString() != "") {
            parts.add(currentPart.toString())
        }
        return parts
    }
}
