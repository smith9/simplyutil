package simplygroovy.util

/**
 * Created: 21/12/11
 * @author david
 */
class TimeThis {
    static long SECOND = 1000
    static long MINUTE = 60 * SECOND
    static long HOUR = 60 * MINUTE
    static Closure time = { Closure timeThis ->
        long start = System.nanoTime()
        timeThis()
        long milli = toMilli(System.nanoTime() - start)
        return milli
    }

    static long toMilli(long nanoseconds) {
        return nanoseconds / 1000000
    }

    static String human(long milli) {
        switch(milli) {
            case 0..SECOND-1:
                return "$milli milli"
            case SECOND..MINUTE-1:
                float seconds = milli / SECOND
                return "${seconds}s"
            case MINUTE:
                return "1m"
            case MINUTE..HOUR-1:
                int minutes = milli / MINUTE
                float seconds = (milli % MINUTE)/SECOND
                return "${minutes}m ${seconds}s"
            case HOUR:
                return "1h"
            default:
                int hours = milli / HOUR
                milli = milli % HOUR
                int minutes = milli / MINUTE
                milli = milli % MINUTE
                float seconds = milli / SECOND
                return "${hours}h ${minutes}m ${seconds}s"

        }
    }
}
