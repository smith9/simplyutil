package simplygroovy.util

import org.joda.time.DateTime
import simplygroovy.bean.Period

/**
 * Created: 28/03/11
 * @author david
 */
class DateUtil {
    public static String FORMAT_WITH_SECONDS = "yyyy-MM-dd_HH_mm_ss"
    /**
     * Create a string for the date that can be used in a filename
     * format is yyyy-MM-dd_HH_mm_ss
     * @param date
     * @return
     */
    public static String asFilenameString(DateTime date) {
        return date.toString(FORMAT_WITH_SECONDS)
    }

    public static DateTime level(DateTime date, Period period) {
        DateTime levelled = date
        switch (period) {
            case Period.YEAR:
                levelled = levelled.withMonthOfYear(1)
                levelled = levelled.monthOfYear().roundFloorCopy()
                break
            case Period.MONTH:
                levelled = levelled.monthOfYear().roundFloorCopy()
                break
            case Period.DAY:
                levelled = levelled.dayOfMonth().roundFloorCopy()
                break
            case Period.HOUR:
                levelled = levelled.hourOfDay().roundFloorCopy()
                break
            case Period.MINUTE:
                levelled = levelled.minuteOfHour().roundFloorCopy()
                break
        }
        return levelled
    }
    public static DateTime level(Date date, Period period) {
        return level(new DateTime(date), period)
    }

}
