package simplygroovy.csv

/**
 * Created: 23/05/12
 * @author david
 */
class DefaultRow implements Row {
    LinkedHashMap<String, String> row = [:]
    DefaultRow(LinkedHashMap<String, String> row) {
        this.row = row
    }

    String getValue(String column) {
        return row[column]
    }

    List<String> getValues() {
        return row.values() as List
    }

    LinkedHashMap<String, String> getValuesMap() {
        return row
    }
}
