package simplygroovy.csv

/**
 * Created: 23/05/12
 * @author david
 */
public interface Header {
    public Integer indexOf(String column)
    public boolean containsColumn(String column)
    public List<String> getColumns()
}