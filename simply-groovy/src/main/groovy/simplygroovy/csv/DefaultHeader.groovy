package simplygroovy.csv

/**
 * Created: 23/05/12
 * @author david
 */
class DefaultHeader implements Header {
    private Map<String, Integer> columnsByIndex = [:]
    DefaultHeader(List<String> columns) {
        this.columnsByIndex = CsvUtil.calcIndices(columns)
    }

    Integer indexOf(String column) {
        return columnsByIndex[column]
    }

    boolean containsColumn(String column) {
        return columnsByIndex.containsKey(column)
    }

    List<String> getColumns() {
        return columnsByIndex.keySet() as List
    }
}
