package simplygroovy.csv

/**
 * Created: 23/05/12
 * @author david
 */
public interface Row {
    public String getValue(String column)
    public List<String> getValues()
    public LinkedHashMap<String, String> getValuesMap()
}