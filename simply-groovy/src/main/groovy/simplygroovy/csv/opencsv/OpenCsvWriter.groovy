package simplygroovy.csv.opencsv

import simplygroovy.csv.CsvWriter
import au.com.bytecode.opencsv.CSVWriter

/**
 * Created: 24/03/11
 * @author david
 */
class OpenCsvWriter implements CsvWriter{
    private au.com.bytecode.opencsv.CSVWriter writer

    public OpenCsvWriter(Writer writer) {
        this.writer = new au.com.bytecode.opencsv.CSVWriter(new BufferedWriter(writer))
    }

    public OpenCsvWriter(File csvFile) {
        this(new FileWriter(csvFile))
    }

    void writeRow(List<String> row) {
        writer.writeNext(row as String[])
    }

    void close() {
        writer.close()
    }
}
