package simplygroovy.csv.opencsv

import au.com.bytecode.opencsv.CSVReader
import simplygroovy.csv.CsvReader
import simplygroovy.csv.CsvUtil
import simplygroovy.csv.Header
import simplygroovy.csv.DefaultHeader
import simplygroovy.csv.Row
import simplygroovy.csv.DefaultRow

/**
 * Created: Mar 15, 2011
 * @author david
 */
class OpenCsvReader implements CsvReader {
    au.com.bytecode.opencsv.CSVReader reader
    List<String> header
    List<String> currentLine
    Map<String, Integer> columnIndices

    public OpenCsvReader(Reader read) {
        reader = new au.com.bytecode.opencsv.CSVReader(read)
        header = reader.readNext() as List<String>
        calcIndices()
    }

    public OpenCsvReader(File csvFile) {
        this(new FileReader(csvFile))
    }

    private void calcIndices() {
        columnIndices = CsvUtil.calcIndices(header)
    }

    public List<String> getHeader() {
        return header
    }

    public List<String> readNext() {
        currentLine = reader.readNext() as List<String>
        return currentLine
    }

    public LinkedHashMap<String, String> readNextAsMap() {
        readNext()
        if (currentLine != null) {
            return CsvUtil.toMap(header, currentLine)
        }
        return null;
    }

    public String getCurrentValue(String column) {
        Integer index = columnIndices[column]
        if (index == null) {
            throw new IllegalArgumentException("Column $column was not present in the header or the header was not present")
        }
        return currentLine[index]
    }

    public void eachRowMapWithIndex(Closure processRow) {
        Header header = new DefaultHeader(getHeader())
        if(header.columns.isEmpty()) {
            return
        }
        LinkedHashMap<String, String> rowMap
        int index = 0
        while ((rowMap = readNextAsMap()) != null) {
            Row row = new DefaultRow(rowMap)
            processRow(header, row, index)
            index++
        }
    }
}
