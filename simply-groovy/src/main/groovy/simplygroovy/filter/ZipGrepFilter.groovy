package simplygroovy.filter

import java.util.zip.ZipInputStream
import java.util.zip.ZipEntry
import org.apache.commons.io.IOUtils

/**
 * User: david
 * Date: 15/11/12
 * Time: 10:16 PM
 */
class ZipGrepFilter implements StreamFilter {
    PrettyPrintFilter prettyFilter
    GrepFilter grepFilter

    ZipGrepFilter(String regex, boolean printFilenames = true) {
        prettyFilter = new PrettyPrintFilter()
        grepFilter = new GrepFilter(regex)
        grepFilter.printInputName = printFilenames
    }
    /**
     * Remember to close the input stream you pass
     * @param zipInputStream
     * @param filter
     */
    @Override
    InputStream filterStream(InputStream input) {
        ZipInputStream zipStream = checkZipInputStreamPassed(input)
        ZipEntry entry = null
        PipedOutputStream startOfPipe = new PipedOutputStream()
        PipedInputStream endOfPipe = new PipedInputStream(startOfPipe)

        while ((entry = zipStream.getNextEntry()) != null) {
            String entryName = entry.getName()
            filterEntry(entryName, zipStream, startOfPipe)
            zipStream.closeEntry()
        }
        startOfPipe.flush()
        startOfPipe.close()
        return endOfPipe
    }

    private void filterEntry(String entryName, ZipInputStream zipStream, PipedOutputStream filterAllEntriesToThisStream) {
        ChainedFilter filters = createFilters(entryName)
        InputStream result = filters.filterStream(zipStream)
        IOUtils.copy(result, filterAllEntriesToThisStream)
    }

    private ChainedFilter createFilters(String entryName) {
        ChainedFilter filters = new ChainedFilter()
        if (entryName.endsWith("xml")) {
            filters.add(prettyFilter)
        }

        grepFilter.inputName = entryName
        filters.add(grepFilter)
        return filters
    }

    private ZipInputStream checkZipInputStreamPassed(InputStream input) {
        if (!(input instanceof ZipInputStream)) {
            throw new IllegalArgumentException("Must pass a ZipInputStream, you passed a ${input.class}")
        }

        return input as ZipInputStream
    }



}
