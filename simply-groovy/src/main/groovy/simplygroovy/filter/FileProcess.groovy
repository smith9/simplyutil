package simplygroovy.filter

/**
 * User: david
 * Date: 15/11/12
 * Time: 10:16 PM
 */
public interface FileProcess {
    InputStream process(File file, StreamFilter filter)
}