package simplygroovy.filter

import java.util.regex.Pattern

/**
 * User: david
 * Date: 15/11/12
 * Time: 8:52 PM
 */
class GrepFilter implements StreamFilter {

    GrepFilter(Pattern pattern) {
        this.pattern = pattern
    }
    GrepFilter(String regex) {
        this.pattern = Pattern.compile(regex)
    }
    boolean printInputName = true
    Pattern pattern
    String inputName = ""
    @Override
    public InputStream filterStream(InputStream input) {
        PipedOutputStream startOfPipe = new PipedOutputStream()
        PipedInputStream endOfPipe = new PipedInputStream(startOfPipe)
        grep(input, startOfPipe)
        startOfPipe.flush()
        startOfPipe.close()
        return endOfPipe
    }

    void grep(InputStream input, OutputStream output) {
        int lineNumber = 1
        input.eachLine { String line ->
            if(pattern.matcher(line).find()) {
                if(printInputName) {
                    if(!inputName) {
                        throw new IllegalStateException("No inputname was specified, call setInputName or printInputName=false")
                    }
                    output.write(("$inputName($lineNumber): " as String).bytes)
                }
                output.write((line + System.getProperty("line.separator")).bytes)
            }
            lineNumber++
        }
    }
}
