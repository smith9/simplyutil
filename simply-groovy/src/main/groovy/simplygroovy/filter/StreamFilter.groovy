package simplygroovy.filter

/**
 * User: david
 * Date: 15/11/12
 * Time: 8:48 PM
 */
public interface StreamFilter {
    InputStream filterStream(InputStream input)
}