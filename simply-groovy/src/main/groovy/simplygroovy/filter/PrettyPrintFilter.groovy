package simplygroovy.filter

import simplygroovy.prettyprint.PrettyPrint

/**
 * User: david
 * Date: 15/11/12
 * Time: 8:51 PM
 */
class PrettyPrintFilter implements StreamFilter {
    @Override
    public InputStream filterStream(InputStream input) {
        PipedOutputStream startOfPipe = new PipedOutputStream()
        PipedInputStream endOfPipe = new PipedInputStream(startOfPipe)
        new PrettyPrint().prettyPrint(input, startOfPipe)
        startOfPipe.flush()
        startOfPipe.close()
        return endOfPipe
    }
}
