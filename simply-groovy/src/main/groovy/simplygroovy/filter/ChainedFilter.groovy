package simplygroovy.filter

import org.apache.commons.io.IOUtils

/**
 * User: david
 * Date: 15/11/12
 * Time: 10:33 PM
 */
class ChainedFilter implements StreamFilter {
    List<StreamFilter> filters = []

    void add(StreamFilter filter) {
        filters.add(filter)
    }

    void addAll(List<StreamFilter> filters) {
        filters.addAll(filters)
    }
    @Override
    void filter(InputStream firstInput, OutputStream finalOutput) {
        OutputStream output
        InputStream input = firstInput
        filters.each { StreamFilter filter ->
            output = new ByteArrayOutputStream()
            filter.filterStream(input, output)
            input = new ByteArrayInputStream()
            IOUtils.copy(output, input)
        }
    }

    @Override
    InputStream filterStream(InputStream first) {
        InputStream input = first
        InputStream next
        filters.each { StreamFilter filter ->
            next = filter.filterStream(input)
            input = next
        }
        return next
    }
}
