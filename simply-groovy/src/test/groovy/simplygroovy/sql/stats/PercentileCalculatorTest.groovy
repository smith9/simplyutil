package simplygroovy.sql.stats;


import org.junit.Test
import static org.junit.Assert.assertEquals

/**
 * Created: 25/06/11
 *
 * @author david
 */
public class PercentileCalculatorTest {
    @Test
    public void testCalcPercentileRowsLessThanCount() throws Exception {
        AggregateStats stats1 = new AggregateStats()
        stats1.precisionOfPercentileKey = 1
        stats1.count = 1
        List<AggregateStats> statsList = [stats1]
        List<Double> percentiles = [0]
        List<Integer> rows = new PercentileCalculator(statsList, percentiles).calcPercentileRows()
        assertEquals(1, rows.size())
        assertEquals(1, rows[0])
    }
    @Test
    public void testCalcPercentileFewRows() throws Exception {
        AggregateStats stats1 = new AggregateStats()
        stats1.precisionOfPercentileKey = 1
        stats1.count = 3
        List<AggregateStats> statsList = [stats1]
        List<Double> percentiles = [0, 25, 50, 75, 95, 100]
        List<Integer> rows = new PercentileCalculator(statsList, percentiles).calcPercentileRows()
        assertEquals(6, rows.size())
        assertEquals([1, 1, 2, 3, 3, 3], rows)
    }
    @Test
    public void testCalcPercentileRows() throws Exception {
        AggregateStats stats1 = new AggregateStats()
        stats1.precisionOfPercentileKey = 1
        stats1.count = 100
        List<AggregateStats> statsList = [stats1]
        List<Double> percentiles = [0, 25, 50, 75, 95, 100]
        List<Integer> rows = new PercentileCalculator(statsList, percentiles).calcPercentileRows()
        assertEquals(10, rows.size())
        assertEquals([1, 25, 26, 50, 51, 75, 76, 95, 96, 100], rows)
    }
    @Test
    public void testCalcPercentileRowsMultipleStats() throws Exception {
        AggregateStats stats1 = new AggregateStats()
        AggregateStats stats2 = new AggregateStats()
        stats1.precisionOfPercentileKey = 1
        stats1.count = 100
        stats2.count = 1000
        List<AggregateStats> statsList = [stats1,stats2]
        List<Double> percentiles = [0, 25, 50, 75, 95, 100]
        List<Integer> rows = new PercentileCalculator(statsList, percentiles).calcPercentileRows()
        assertEquals(20, rows.size())
        assertEquals([1, 25, 26, 50, 51, 75, 76, 95, 96, 100, 101, 350, 351, 600, 601, 850, 851, 1050, 1051, 1100], rows)
    }

    @Test
    public void testAddPercentileStatsSingleStats() throws Exception {
        AggregateStats stats1 = new AggregateStats()
        stats1.precisionOfPercentileKey = 1
        stats1.count = 100
        List<AggregateStats> statsList = [stats1]
        List<Double> percentiles = [0, 95, 100]
        new PercentileCalculator(statsList, percentiles).addPercentileStats([1:1.5,95:95.5,96:96.5,100:100.5])
        double smallDelta = 0.0001
        assertEquals(1.5, stats1.getPercentile(0), smallDelta)
        assertEquals(96.45, stats1.getPercentile(95), smallDelta)
        assertEquals(100.5, stats1.getPercentile(100), smallDelta)
    }
    @Test
    public void testAddPercentileStatsMissingRow() throws Exception {
        AggregateStats stats1 = new AggregateStats()
        stats1.precisionOfPercentileKey = 1
        stats1.count = 100
        List<AggregateStats> statsList = [stats1]
        List<Double> percentiles = [0, 95, 100]
        new PercentileCalculator(statsList, percentiles).addPercentileStats([1:1.5,95:95.5,96:96.5])
        double smallDelta = 0.0001
        assertEquals(1.5, stats1.getPercentile(0), smallDelta)
        assertEquals(96.45, stats1.getPercentile(95), smallDelta)
        assertEquals(Double.NaN, stats1.getPercentile(100), smallDelta)
    }
    @Test
    public void testAddPercentileStatsMultipleStats() throws Exception {
        AggregateStats stats1 = new AggregateStats()
        AggregateStats stats2 = new AggregateStats()
        stats1.precisionOfPercentileKey = 1
        stats1.count = 100
        stats2.count = 1000
        List<AggregateStats> statsList = [stats1,stats2]
        List<Double> percentiles = [0, 95, 100]
        new PercentileCalculator(statsList, percentiles).addPercentileStats([1:1.5,95:95.5,96:96.5,100:100.5, 101:101.5, 1050:1050.5, 1051:1051.5, 1100:1100.5])
        double smallDelta = 0.0001
        assertEquals(1.5, stats1.getPercentile(0), smallDelta)
        assertEquals(96.45, stats1.getPercentile(95), smallDelta)
        assertEquals(100.5, stats1.getPercentile(100), smallDelta)
        assertEquals(101.5, stats2.getPercentile(0), smallDelta)
        assertEquals(1051.45, stats2.getPercentile(95), smallDelta)
        assertEquals(1100.5, stats2.getPercentile(100), smallDelta)
    }
}
