package simplygroovy.sql.stats;


import groovy.sql.Sql
import org.junit.Test
import simplygroovy.sql.ConnectionUtil
import simplygroovy.sql.query.H2
import simplygroovy.sql.query.Select
import static org.junit.Assert.assertEquals
import simplygroovy.csv.CsvReader
import simplygroovy.csv.opencsv.OpenCsvReader
import simplygroovy.bean.MapToBean
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.junit.BeforeClass

/**
 * Created: 23/06/11
 *
 * @author david
 */
public class AggregateQueryRunnerTest {
    ArrayList<Integer> PERCENTILES = [1, 25, 50, 75, 95, 100]

    Map<String, Float> deltas = ["STDDEV_POP(TIMING)": 0.5]
    @BeforeClass
    public static void setupClass() {
        System.setProperty("h2.identifiersToUpper","false")
    }
    @Test
    public void testCalculatePercentiles() throws Exception {
        File settingsDir = new File(getClass().getClassLoader().getResource('stats').toURI())
        File testDbTable = new File(getClass().getClassLoader().getResource('stats/stats.csv').toURI())
        Sql sql = ConnectionUtil.createConnection("testH2", new File(settingsDir, "Credentials.groovy"))
        createTableFromCsv(sql, "test", testDbTable)
        AggregateQueryRunner query = new AggregateQueryRunner(createSelect(), sql)
        List<AggregateStats> stats = query.calculatePercentiles("Timing", PERCENTILES)
        assertEquals(4, stats.size())
        List<AggregateStats> expected = expectedStats(testDbTable)
        (0..3).each { checkStats(expected[it], stats[it])}

    }
    @Test
    public void testCalculatePercentilesNoResults() throws Exception {
        File settingsDir = new File(getClass().getClassLoader().getResource('stats').toURI())
        Sql sql = ConnectionUtil.createConnection("testH2", new File(settingsDir, "Credentials.groovy"))
        createEmptyTable(sql, "test")
        AggregateQueryRunner query = new AggregateQueryRunner(createSelect(), sql)
        List<AggregateStats> stats = query.calculatePercentiles("Timing", PERCENTILES)
        assertEquals(0, stats.size())
    }

    void checkStats(AggregateStats expected, AggregateStats actual) {
        actual.columnNames().each { String column ->
            Object expectedValue = expected.get(column)
            Object actualValue = actual.get(column)
            assertEquals("checking $column", expectedValue, actualValue)
        }
        double smallDelta = 0.0001
        PERCENTILES.each { int percentile ->
            assertEquals(expected.getPercentile(percentile), actual.getPercentile(percentile), smallDelta)
        }
        assertEquals(expected.average, actual.average, smallDelta)
        assertEquals(expected.min, actual.min, smallDelta)
        assertEquals(expected.max, actual.max, smallDelta)
        assertEquals(expected.count, actual.count)
        assertEquals(expected.stdDevPop, actual.stdDevPop, 0.5)
    }

    Select createSelect() {
        Select select = new Select(new H2())
        select.with {
            columns(["car", "route"]).
                count("timing", "count_timing").
                max("timing", "max_timing").
                min("timing", "min_timing").
                avg("timing", "avg_timing").
                stdDevPop("timing", "stddev_pop_timing")
            from("test")
            groupBy("car", "route")
            orderBy("car", "route")
        }
        return select
    }

    private void createTableFromCsv(Sql sql, String tableName, File csv) {
        System.setProperty("h2.identifiersToUpper","false")
        sql.execute("drop table if exists ${Sql.expand tableName};")
        sql.execute("create table ${Sql.expand tableName}(car VARCHAR(20), route VARCHAR(20), timing FLOAT, index INT) as select * from CSVREAD('${Sql.expand csv.absolutePath}');")
    }
    private void createEmptyTable(Sql sql, String tableName) {
        sql.execute("drop table if exists ${Sql.expand tableName};")
        sql.execute("create table ${Sql.expand tableName}(car VARCHAR(20), route VARCHAR(20), timing FLOAT, index INT);")
    }

    private List<AggregateStats> expectedStats(File csv) {
        List<Travel> travels = loadTestData(csv)
        Map<String, List<Travel>> grouped = travels.groupBy { Travel travel ->
            return travel.car + ":" + travel.route
        }
        grouped = grouped.sort { it.key }
        List<AggregateStats> expected = []
        grouped.each { String group, List<Travel> groupTravels ->
            double[] timings = groupTravels.collect { Travel travel -> travel.timing} as double[]
            AggregateStats aggregate = creatStatsUsingApache(timings, group)
            expected.add(aggregate)
        }
        return expected
    }

    private AggregateStats creatStatsUsingApache(double[] timings, String group) {
        DescriptiveStatistics stats = new DescriptiveStatistics(timings)
        AggregateStats aggregate = new AggregateStats()
        String[] groupColumns = group.split(":")
        aggregate.add("car", groupColumns[0])
        aggregate.add("route", groupColumns[1])
        aggregate.count = stats.getN()
        aggregate.min = stats.getMin()
        aggregate.max = stats.getMax()
        aggregate.average = stats.getMean()
        aggregate.stdDevPop = stats.getStandardDeviation()
        aggregate.setPercentile(1, stats.getPercentile(1))
        aggregate.setPercentile(25, stats.getPercentile(25))
        aggregate.setPercentile(50, stats.getPercentile(50))
        aggregate.setPercentile(75, stats.getPercentile(75))
        aggregate.setPercentile(95, stats.getPercentile(95))
        aggregate.setPercentile(100, stats.getPercentile(100))
        return aggregate
    }

    private List<Travel> loadTestData(File csv) {
        CsvReader reader = new OpenCsvReader(csv)
        List<Travel> data = []
        LinkedHashMap<String, String> map
        while ((map = reader.readNextAsMap()) != null) {
            Travel travel = new MapToBean().convert(Travel.class, map)
            data.add(travel)
        }
        return data
    }

}
