package simplygroovy.sql.stats;


import org.junit.Test
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNull

/**
 * Created: 8/06/12
 *
 * @author david
 */
public class DescriptiveStatsWrapperTest {
    @Test
    public void testGetNonExistentEntry() throws Exception {
        DescriptiveStatsWrapper stat = new DescriptiveStatsWrapper(new DescriptiveStatistics())
        assertNull("", stat.get(""))
    }
    @Test
    public void testGetExistingEntry() throws Exception {
        DescriptiveStatsWrapper stat = new DescriptiveStatsWrapper(new DescriptiveStatistics())
        stat.add("ABC", Boolean.FALSE)
        assertEquals(Boolean.FALSE, stat.get("ABC"))
    }
    @Test
    public void testJoinColumns() throws Exception {
        DescriptiveStatsWrapper stat = new DescriptiveStatsWrapper(new DescriptiveStatistics())
        stat.add("ABC", Boolean.FALSE)
        stat.add("DEF", "David")
        assertEquals("false:David", stat.joinColumnValues())
    }

    @Test
    public void testGetPercentileKeys() throws Exception {
        DescriptiveStatsWrapper stat = new DescriptiveStatsWrapper(new DescriptiveStatistics())
        assertEquals((1..100).collect {it} as Set<Integer>, stat.percentileKeys())

    }
}
