package simplygroovy.sql.stats;


import org.junit.Test
import static org.junit.Assert.assertTrue

/**
 * Created: 27/01/12
 *
 * @author david
 */
public class StatsUtilTest {
    @Test
    public void testCollectColumnValues() throws Exception {
        AggregateStats stats = new AggregateStats()
        stats.add('product', 'geiger')
        stats.add('subproduct', 'product1')
        stats.add('failure', Boolean.TRUE)
        stats.add('batch', Boolean.TRUE)
        stats.add('batchType', '')
        AggregateStats stats2 = new AggregateStats()
        stats2.add('product', 'geiger')
        stats2.add('failure', Boolean.FALSE)
        stats2.add('subproduct', 'product2')
        Map<String, Set<String, Object>> values = StatsUtil.collectColumnValues([stats, stats2])
        assert ['geiger'] as Set<Object> == values['product']
        assert ['product1', 'product2'] as Set<Object> == values['subproduct']
        assert [Boolean.FALSE, Boolean.TRUE] as Set<Object> == values['failure']
        assert [Boolean.TRUE] as Set<Object> == values['batch']

    }
    @Test
    public void testCollectColumnValuesEmpty() throws Exception {
        assertTrue(StatsUtil.collectColumnValues([]).isEmpty())
    }
}
