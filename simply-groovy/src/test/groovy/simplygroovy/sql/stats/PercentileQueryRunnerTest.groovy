package simplygroovy.sql.stats;


import groovy.sql.Sql
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.junit.BeforeClass
import org.junit.Test
import simplygroovy.bean.MapToBean
import simplygroovy.csv.CsvReader
import simplygroovy.csv.opencsv.OpenCsvReader
import simplygroovy.sql.ConnectionUtil
import simplygroovy.sql.query.H2
import simplygroovy.sql.query.Select
import static org.junit.Assert.assertEquals

/**
 * Created: 23/06/11
 *
 * @author david
 */
public class PercentileQueryRunnerTest {
    ArrayList<Integer> PERCENTILES = [1, 25, 50, 75, 95, 100]

    Map<String, Float> deltas = ["STDDEV_POP(TIMING)": 0.5]
    @BeforeClass
    public static void setupClass() {
        System.setProperty("h2.identifiersToUpper","false")
    }
    @Test
    public void testCalculate() throws Exception {
        File settingsDir = new File(getClass().getClassLoader().getResource('stats').toURI())
        File testDbTable = new File(getClass().getClassLoader().getResource('stats/stats.csv').toURI())
        Sql sql = ConnectionUtil.createConnection("testH2", new File(settingsDir, "Credentials.groovy"))
        createTableFromCsv(sql, "test", testDbTable)
        PercentileQueryRunner query = new PercentileQueryRunner(createSelect(), sql, "Timing")
        List<Statistics> stats = query.calculate()
        assertEquals(4, stats.size())
        List<Statistics> expected = expectedStats(testDbTable)
        (0..3).each { checkStats(expected[it], stats[it])}

    }
    @Test
    public void testCalculateNoResults() throws Exception {
        File settingsDir = new File(getClass().getClassLoader().getResource('stats').toURI())
        Sql sql = ConnectionUtil.createConnection("testH2", new File(settingsDir, "Credentials.groovy"))
        createEmptyTable(sql, "test")
        PercentileQueryRunner query = new PercentileQueryRunner(createSelect(), sql, "Timing")
        List<Statistics> stats = query.calculate()
        assertEquals(0, stats.size())
    }

    void checkStats(Statistics expected, Statistics actual) {
        expected.columnNames().each { String column ->
            Object expectedValue = expected.get(column)
            Object actualValue = actual.get(column)
            assertEquals("checking $column", expectedValue, actualValue)
        }
        double smallDelta = 0.0001
        PERCENTILES.each { int percentile ->
            assertEquals("cehcking percentile $percentile", expected.getPercentile(percentile), actual.getPercentile(percentile), smallDelta)
        }
        assertEquals(expected.average, actual.average, smallDelta)
        assertEquals(expected.min, actual.min, smallDelta)
        assertEquals(expected.max, actual.max, smallDelta)
        assertEquals(expected.count, actual.count)
        assertEquals(expected.stdDevPop, actual.stdDevPop, 0.5)
    }

    Select createSelect() {
        Select select = new Select(new H2())
        select.with {
            columns(["car", "route", "timing"])
            from("test")
        }
        return select
    }

    private void createTableFromCsv(Sql sql, String tableName, File csv) {
        System.setProperty("h2.identifiersToUpper","false")
        sql.execute("drop table if exists ${Sql.expand tableName};")
        sql.execute("create table ${Sql.expand tableName}(car VARCHAR(20), route VARCHAR(20), timing FLOAT, index INT) as select * from CSVREAD('${Sql.expand csv.absolutePath}');")
    }
    private void createEmptyTable(Sql sql, String tableName) {
        sql.execute("drop table if exists ${Sql.expand tableName};")
        sql.execute("create table ${Sql.expand tableName}(car VARCHAR(20), route VARCHAR(20), timing FLOAT, index INT);")
    }

    private List<Statistics> expectedStats(File csv) {
        List<Travel> travels = loadTestData(csv)
        Map<String, List<Travel>> grouped = travels.groupBy { Travel travel ->
            return travel.car + ":" + travel.route
        }
        grouped = grouped.sort { it.key }
        List<Statistics> expected = []
        grouped.each { String group, List<Travel> groupTravels ->
            double[] timings = groupTravels.collect { Travel travel -> travel.timing} as double[]
            Statistics aggregate = creatStatsUsingApache(timings, group)
            expected.add(aggregate)
        }
        return expected
    }

    private Statistics creatStatsUsingApache(double[] timings, String group) {
        DescriptiveStatistics stats = new DescriptiveStatistics(timings)
        DescriptiveStatsWrapper aggregate = new DescriptiveStatsWrapper(stats)
        String[] groupColumns = group.split(":")
        aggregate.add("car", groupColumns[0])
        aggregate.add("route", groupColumns[1])
        return aggregate
    }

    private List<Travel> loadTestData(File csv) {
        CsvReader reader = new OpenCsvReader(csv)
        List<Travel> data = []
        LinkedHashMap<String, String> map
        while ((map = reader.readNextAsMap()) != null) {
            Travel travel = new MapToBean().convert(Travel.class, map)
            data.add(travel)
        }
        return data
    }

}
