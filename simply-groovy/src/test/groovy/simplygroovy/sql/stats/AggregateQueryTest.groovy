package simplygroovy.sql.stats;


import org.junit.Test
import groovy.sql.Sql
import simplygroovy.sql.ConnectionUtil
import simplygroovy.sql.query.Select
import simplygroovy.sql.query.H2

import static org.junit.Assert.assertEquals

/**
 * Created: 20/06/11
 *
 * @author david
 */
public class AggregateQueryTest {

    @Test
    public void testCalculatePercentiles() throws Exception {
        AggregateQuery query = new AggregateQuery()
        Select percentileQuery = query.createQuery(createSelect(), "Timing", [1, 2, 3])
        String expected = """SELECT RankQuery.*
FROM (SELECT ROWNUM() AS rank,
OrderQuery.*
FROM (SELECT Car,
Route,
Timing
FROM test
ORDER BY Car, Route, Timing) AS OrderQuery) AS RankQuery
WHERE rank BETWEEN 1
AND 1
OR rank BETWEEN 2
AND 2
OR rank BETWEEN 3
AND 3;"""
        assertEquals(expected, percentileQuery.asString())
    }

    Select createSelect() {
        Select select = new Select(new H2())
        select.with {
            columns(["Car","Route"]).
                count("Timing").
                max("Timing").
                min("Timing").
                avg("Timing").
                stdDevPop("Timing").
                stdDevSample("Timing")
            from("test")
            groupBy("Car","Route")
        }
        return select
    }
}
