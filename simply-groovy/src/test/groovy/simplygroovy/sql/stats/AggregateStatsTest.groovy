package simplygroovy.sql.stats

import org.junit.Test
import static org.junit.Assert.assertEquals
import java.util.Map.Entry

/**
 * Created: 23/01/12
 * @author david
 */
class AggregateStatsTest {
    @Test
    public void testColumns() {
        AggregateStats stats = new AggregateStats()
        assert [] as Set<String> == stats.columnNames()
        stats.add('forename', 'david')
        stats.add('surname', 'smith')
        assert ['forename', 'surname'] as Set<String> == stats.columnNames()
    }
    @Test
    public void testPercentiles() {
        AggregateStats stats = new AggregateStats()

        stats.setPercentile(1, 1.5)
        assertEquals([1] as Set<String>, stats.percentileKeys())

        stats.setPercentile(99, 99.5)
        assertEquals([1,99] as Set<String>, stats.percentileKeys())

        assertEquals(1.5d, stats.getPercentile(1), 0.00005d)
        assertEquals(99.5d, stats.getPercentile(99), 0.00005d)
    }

    @Test
    public void testStats() {
        AggregateStats stats = new AggregateStats()

        stats.setMin(1)
        assertEquals(1.0, stats.getMin(), 0.00005)

        stats.setMax(123)
        assertEquals(123.0, stats.getMax(), 0.00005)

        stats.setCount(12)
        assertEquals(12, stats.getCount())

        stats.setAverage(55.5)
        assertEquals(55.5, stats.getAverage(), 0.00005)

        stats.setStdDevPop(12.3)
        assertEquals(12.3, stats.getStdDevPop(), 0.00005)

        stats.setStdDevSample(12.5)
        assertEquals(12.5, stats.getStdDevSample(), 0.00005)

        stats.setSum(100)
        assertEquals(100.0, stats.getSum(), 0.00005)
    }
}
