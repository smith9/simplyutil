package simplygroovy.sql.query;


import org.junit.Test
import static org.junit.Assert.assertEquals

/**
 * Created: 19/06/11
 *
 * @author david
 */
public class JoinTest {
    @Test
    public void testJoinNoAlias() throws Exception {
        Join join = new Join("table").setConvert(new H2())
        join.on("id1", "id2")
        assertEquals("JOIN table ON id1 = id2", join.asString(" "))
    }
    @Test
    public void testJoinNoOn() throws Exception {
        Join join = new Join("table").setConvert(new H2())
        assertEquals("JOIN table", join.asString(" "))
    }
    @Test
    public void testJoinAliasAndOn() throws Exception {
        Join join = new Join("table", "t").setConvert(new H2())
        join.on("id1", "id2")
        assertEquals("JOIN table AS t ON id1 = id2", join.asString(" "))
    }
    @Test
    public void testJoinAlias() throws Exception {
        Join join = new Join("table", "t").setConvert(new H2())
        assertEquals("JOIN table AS t", join.asString(" "))
    }
    @Test
    public void testJoinAliasSchemaMySql() throws Exception {
        Join join = new Join("table", "t").setSchema("geiger").setConvert(new MySql())
        assertEquals("JOIN geiger.table AS t", join.asString(" "))
    }
    @Test
    public void testJoinAliasSchemaH2() throws Exception {
        Join join = new Join("table", "t").setSchema("geiger").setConvert(new H2())
        assertEquals("JOIN table AS t", join.asString(" "))
    }
}
