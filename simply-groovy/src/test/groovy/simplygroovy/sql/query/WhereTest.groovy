package simplygroovy.sql.query

import org.junit.Test
import static org.junit.Assert.assertEquals
import org.junit.rules.ExpectedException
import org.junit.Rule
import org.joda.time.DateTime

/**
 * Created: 18/06/11
 * @author david
 */
class WhereTest {
    @Rule
    public ExpectedException thrown = new ExpectedException();
    static final Closure<Where> NOT_EQUAL = { Where where -> return where.notEqual() }
    static final Closure<Where> EQUAL = { Where where -> return where.equal() }
    static final Closure<Where> LESS_THAN = { Where where -> return where.lessThan() }
    static final Closure<Where> LESS_THAN_EQUAL = { Where where -> return where.lessThanEqual() }
    static final Closure<Where> GREATER_THAN_EQUAL = { Where where -> return where.greaterThanEqual() }
    static final Closure<Where> GREATER_THAN = { Where where -> return where.greaterThan() }
    static final Closure<Where> IN_LIST = { Where where -> return where.inList() }

    @Test
    public void testCloneAnd() {
        String clause = new Where(new MySql(), Logical.And, "fullname").like("David%").clone().asString()
        assertEquals("AND fullname LIKE 'David%'", clause)
    }
    @Test
    public void testCloneOr() {
        String clause = new Where(new MySql(), Logical.Or, "fullname").like("David%").clone().asString()
        assertEquals("OR fullname LIKE 'David%'", clause)
    }
    @Test
    public void testIntValues() {
        String clause = new Where(new MySql(), "amount").inList().value(-1, 18, 21, 65).asString()
        assertEquals("WHERE amount IN (-1, 18, 21, 65)", clause)
    }
    @Test
    public void testDoubleValues() {
        String clause = new Where(new MySql(), "amount").inList().value(-1.5, 18.0, 21.6555555, 123465.999999999).asString()
        assertEquals("WHERE amount IN (-1.5, 18.0, 21.6555555, 123465.999999999)", clause)
    }

    @Test
    public void testSingleValueOp() {
        checkOp("WHERE start <> STR_TO_DATE('2011-06-18', '%Y-%m-%d')", NOT_EQUAL)
        checkOp("WHERE start = STR_TO_DATE('2011-06-18', '%Y-%m-%d')", EQUAL)
        checkOp("WHERE start < STR_TO_DATE('2011-06-18', '%Y-%m-%d')", LESS_THAN)
        checkOp("WHERE start <= STR_TO_DATE('2011-06-18', '%Y-%m-%d')", LESS_THAN_EQUAL)
        checkOp("WHERE start > STR_TO_DATE('2011-06-18', '%Y-%m-%d')", GREATER_THAN)
        checkOp("WHERE start >= STR_TO_DATE('2011-06-18', '%Y-%m-%d')", GREATER_THAN_EQUAL)
    }

    private void checkOp(String expected, Closure<Where> op) {
        Where where = new Where(new MySql(), "start")
        String clause = op(where).date("yyyy-MM-dd", "2011-06-18").asString()
        assertEquals(expected as String, clause)
    }

    @Test
    public void testSingleValueOpTooManyValues() {
        checkOpTooManyValues("Can only specify 1 value for not equal clause.", NOT_EQUAL)
        checkOpTooManyValues("Can only specify 1 value for equal clause.", EQUAL)
        checkOpTooManyValues("Can only specify 1 value for less than clause.", LESS_THAN)
        checkOpTooManyValues("Can only specify 1 value for less that equal clause.", LESS_THAN_EQUAL)
        checkOpTooManyValues("Can only specify 1 value for greater than equal clause.", GREATER_THAN_EQUAL)
        checkOpTooManyValues("Can only specify 1 value for greater than clause.", GREATER_THAN)
    }

    private def checkOpTooManyValues(String expectedMessage, Closure<Where> op) {
        thrown.expect(IllegalArgumentException.class)
        thrown.expectMessage(expectedMessage)
        thrown.expectMessage("start")
        Where where = new Where(new MySql(), "start")
        String clause = op(where).date("yyyy-MM-dd", "2011-06-18", "2011-06-19").asString()
    }

    @Test
    public void testIn() {
        String clause = new Where(new MySql(), "name").inList().value("David", "Mark", "James").asString()
        assertEquals("WHERE name IN ('David', 'Mark', 'James')", clause)
    }

    @Test
    public void testInNoValues() {
        checkNoValuesSpecified("Must specify at least 1 value for IN clause.", IN_LIST)
        checkNoValuesSpecified("Can only specify 1 value for not equal clause.", NOT_EQUAL)
        checkNoValuesSpecified("Can only specify 1 value for equal clause.", EQUAL)
        checkNoValuesSpecified("Can only specify 1 value for less than clause.", LESS_THAN)
        checkNoValuesSpecified("Can only specify 1 value for less that equal clause.", LESS_THAN_EQUAL)
        checkNoValuesSpecified("Can only specify 1 value for greater than equal clause.", GREATER_THAN_EQUAL)
        checkNoValuesSpecified("Can only specify 1 value for greater than clause.", GREATER_THAN)

    }

    private def checkNoValuesSpecified(String expected, Closure<Where> op) {
        thrown.expect(IllegalArgumentException.class)
        thrown.expectMessage(expected)
        thrown.expectMessage("name")
        Where where = new Where(new MySql(), "name")
        op(where).asString()
    }

    @Test
    public void testAnd() {
        String clause = new Where(new MySql(), Logical.And, "fullname").like("David%").asString()
        assertEquals("AND fullname LIKE 'David%'", clause)
    }
    @Test
    public void testOr() {
        String clause = new Where(new MySql(), Logical.Or, "fullname").like("David%").asString()
        assertEquals("OR fullname LIKE 'David%'", clause)
    }
    @Test
    public void testLike() {
        String clause = new Where(new MySql(), "fullname").like("David%").asString()
        assertEquals("WHERE fullname LIKE 'David%'", clause)
    }

    @Test
    public void testLikeTooManyValues() {
        thrown.expect(IllegalArgumentException.class)
        thrown.expectMessage("Can only specify 1 value for LIKE clause.")
        thrown.expectMessage("fullname")
        String clause = new Where(new MySql(), "fullname").like().date("yyyy-MM-dd", "2011-06-18").asString()
    }
    @Test
    public void testNoOperator() {
        thrown.expect(IllegalArgumentException.class)
        thrown.expectMessage("No operator was specified.")
        thrown.expectMessage("fullname")
        new Where(new MySql(), "fullname").date("yyyy-MM-dd", "2011-06-18").asString()
    }

    @Test
    public void testIsNull() {
        String clause = new Where(new MySql(), "start").isNull().asString()
        assertEquals("WHERE start IS NULL", clause)
    }

    @Test
    public void testIsNullTooManyValues() {
        thrown.expect(IllegalArgumentException.class)
        thrown.expectMessage("Can't specify values for IS NULL clause.")
        thrown.expectMessage("start")
        String clause = new Where(new MySql(), "start").isNull().date("yyyy-MM-dd", "2011-06-18").asString()
    }

    @Test
    public void testIsNotNull() {
        String clause = new Where(new MySql(), "start").isNotNull().asString()
        assertEquals("WHERE start IS NOT NULL", clause)
    }

    @Test
    public void testIsNotNullTooManyValues() {
        thrown.expect(IllegalArgumentException.class)
        thrown.expectMessage("Can't specify values for IS NOT NULL clause.")
        thrown.expectMessage("start")
        String clause = new Where(new MySql(), "start").isNotNull().date("yyyy-MM-dd", "2011-06-18").asString()
    }

    @Test
    public void testJodaDate() {
        String clause = new Where(new MySql(), "start").between().date("yyyy-MM-dd", new DateTime("2011-06-18"), new DateTime("2011-06-19")).asString(" ")
        assertEquals("WHERE start BETWEEN STR_TO_DATE('2011-06-18', '%Y-%m-%d') AND STR_TO_DATE('2011-06-19', '%Y-%m-%d')", clause)
    }
    @Test
    public void testBetweenDates() {
        String clause = new Where(new MySql(), "start").between().date("yyyy-MM-dd", "2011-06-18", "2011-06-19").asString(" ")
        assertEquals("WHERE start BETWEEN STR_TO_DATE('2011-06-18', '%Y-%m-%d') AND STR_TO_DATE('2011-06-19', '%Y-%m-%d')", clause)
    }
    @Test
    public void testLiteralValues() {
        String clause = new Where(new MySql(), "start").between().literalValue("SYSDATE()", "SYSDATE() - 1").asString(" ")
        assertEquals("WHERE start BETWEEN SYSDATE() AND SYSDATE() - 1", clause)
    }
    @Test
    public void testColumn() {
        String clause = new Where(new MySql(), "start").equal().column("t2.date").asString(" ")
        assertEquals("WHERE start = t2.date", clause)
    }
    @Test
    public void testToStringSameAsString() {
        Where where = new Where(new MySql(), "start").equal().column("t2.date")
        assertEquals(where.toString(), where.asString())
    }

    @Test
    public void testBetweenTooManyValues() {
        thrown.expect(IllegalArgumentException.class)
        thrown.expectMessage("Can only specify 2 values for BETWEEN clause.")
        thrown.expectMessage("start")
        new Where(new MySql(), "start").between().date("yyyy-MM-dd", "2011-06-18", "2011-06-19", "2011-06-19").asString()
    }
}
