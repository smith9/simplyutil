package simplygroovy.sql.query;


import org.junit.Test
import static org.junit.Assert.assertEquals
import org.junit.rules.ExpectedException
import org.junit.Rule
import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertNull

/**
 * Created: 19/06/11
 *
 * @author david
 */
public class ColumnsTest {
    @Rule
    public ExpectedException thrown = new ExpectedException();

    @Test
    public void testNoColumns() throws Exception {
        thrown.expect(IllegalArgumentException.class)
        thrown.expectMessage("Must specify at least 1 column.")
        Columns column = new Columns().asString()
    }
    @Test
    public void testSimpleNoAsName() throws Exception {
        Columns column = new Columns()
        column.column("*")
        assertEquals("*", column.asString(" "))
    }
    @Test
    public void testSimpleAsName() throws Exception {
        Columns column = new Columns()
        column.column("start_date", "start")
        assertEquals("start_date AS start", column.asString(" "))
    }
    @Test
    public void testMultipleAsName() throws Exception {
        Columns column = new Columns()
        column.column("start_date", "start").column("end_date", "end")
        assertEquals("start_date AS start, end_date AS end", column.asString(" "))
    }
    @Test
    public void testMultipleNoAsName() throws Exception {
        Columns column = new Columns()
        column.column("start_date").column("end_date")
        assertEquals("start_date, end_date", column.asString(" "))
    }
    @Test
    public void testMultipleList() throws Exception {
        Columns column = new Columns("start_date", "end_date")
        assertEquals("start_date, end_date", column.asString(" "))
    }
    @Test
    public void testAddAll() throws Exception {
        ArrayList<String> columns = ["start_date", "end_date"]
        Columns column = new Columns()
        column.addAll(columns)
        assertEquals("start_date, end_date", column.asString(" "))
    }
    @Test
    public void testFindAggregate() throws Exception {
        Columns column = new Columns("name")
        column.max("band", "top_band").max("b", "top_b").max("big").count("*", "num_rows").stdDevPop("d").
           sum("e").stdDevSample("f")
        assertEquals("top_b", column.findAggregate("b", Aggregate.MAX).asName)
    }
    @Test
    public void testGetAggregateColumns() throws Exception {
        Columns column = new Columns("name")
        column.max("band", "top_band").max("b", "top_b").max("big").count("*", "num_rows").stdDevPop("d").
           sum("e").stdDevSample("f")
        List<Column> aggregates = column.getAggregateColumns()
        aggregates.each { Column aggregate ->
            assertNotNull(aggregate.aggregate)
        }
    }

    @Test
    public void testGetNonAggregateColumns() throws Exception {
        Columns column = new Columns("name")
        column.max("band", "top_band").max("b", "top_b").max("big").count("*", "num_rows").stdDevPop("d").
           sum("e").stdDevSample("f")
        List<Column> nonAggregates = column.getNonAggregateColumns()
        nonAggregates.each { Column aggregate ->
            assertNull(aggregate.aggregate)
        }
    }
    @Test
    public void testDbResultName() throws Exception {
        Columns column = new Columns("name")
        column.max("band", "top_band").max("b", "top_b").max("big").count("*").stdDevPop("d").
           sum("e").stdDevSample("f")
        assertEquals("top_b", column.findAggregate("b", Aggregate.MAX).dbResultName())
        assertEquals("MAX(big)", column.findAggregate("big", Aggregate.MAX).dbResultName())
        assertEquals("COUNT(*)", column.findAggregate("*", Aggregate.COUNT).dbResultName())
    }
    @Test
    public void testRemoveAggregates() throws Exception {
        Columns column = new Columns("name")
        column.max("a").min("b").avg("c").count("*", "num_rows").stdDevPop("d").
           sum("e").stdDevSample("f")
        assertEquals("name, MAX(a), MIN(b), AVG(c), COUNT(*) AS num_rows, STDDEV_POP(d), SUM(e), STDDEV_SAMP(f)", column.asString(" "))
        column.removeAggregates()
        assertEquals("name", column.asString(" "))
    }
    @Test
    public void testClone() throws Exception {
        Columns column = new Columns("name")
        column.max("a").min("b").avg("c").count("*", "num_rows").stdDevPop("d").
           sum("e").stdDevSample("f")
        Columns cloned = column.clone()
        column.removeAggregates()
        assertEquals("checking clone", "name, MAX(a), MIN(b), AVG(c), COUNT(*) AS num_rows, STDDEV_POP(d), SUM(e), STDDEV_SAMP(f)", cloned.asString(" "))
        assertEquals("checking original", "name", column.asString(" "))
    }
}
