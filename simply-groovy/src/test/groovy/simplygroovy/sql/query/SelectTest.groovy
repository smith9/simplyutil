package simplygroovy.sql.query

import org.junit.Test
import static org.junit.Assert.assertEquals
import org.junit.rules.ExpectedException
import org.junit.Rule;

/**
 * Created: 19/06/11
 *
 * @author david
 */
public class SelectTest {
    @Rule
    public ExpectedException thrown = new ExpectedException();

    @Test
    public void testNoColumns() {
        thrown.expect(IllegalArgumentException.class)
        thrown.expectMessage("Must specify at least 1 column.")
        new Select(new MySql()).asString()
    }

    @Test
    public void testNoFromTable() {
        thrown.expect(IllegalArgumentException.class)
        thrown.expectMessage("Must specify the from table in the SELECT, call from.")
        Select select = new Select(new MySql())
        select.columns().column("*")
        select.asString()
    }

    @Test
    public void testToStringSameAsString() {
        Select select = new Select(new MySql())
        select.with {
            columns().column("*")
            from("event_duration")
        }
        assertEquals(select.toString(), select.asString())
    }
    @Test
    public void testSimpleSelect() {
        String expected = "SELECT * FROM event_duration;"

        Select select = new Select(new MySql())
        select.with {
            columns().column("*")
            from("event_duration")
        }
        assertEquals(expected, select.asString(" "))
    }
    @Test
    public void testSchemaMySqlSelect() {
        String expected = "SELECT * FROM geiger.event_duration;"

        Convert convert = new MySql()
        Select select = new Select(convert)
        select.with {
            columns().column("*")
            from("event_duration").setSchema("geiger")
        }
        assertEquals(expected, select.asString(" "))
    }
    @Test
    public void testSchemaH2Select() {
        String expected = "SELECT * FROM event_duration;"

        Convert convert = new H2()
        Select select = new Select(convert)
        select.with {
            columns().column("*")
            from("event_duration").setSchema("geiger")
        }
        assertEquals(expected, select.asString(" "))
    }
    @Test
    public void testOrderBy() {
        String expected = "SELECT * FROM event_duration ORDER BY colour DESC, state DESC, shape, duration, name;"

        Select select = new Select(new MySql())
        select.with {
            columns().column("*")
            from("event_duration")
            orderByDesc("colour","state").orderBy("shape").orderBy("duration", "name")
        }
        assertEquals(expected, select.asString(" "))
    }
    @Test
    public void testColumnsCalledManyTimes() {
        String expected = "SELECT surname, forename FROM event_duration;"

        Select select = new Select(new MySql())
        select.with {
            columns().column("surname")
            columns().column("forename")
            from("event_duration")
        }
        assertEquals(expected, select.asString(" "))
    }
    @Test
    public void testColumn() {
        String expected = "SELECT sname, fname AS forename FROM event_duration;"

        Select select = new Select(new MySql())
        select.with {
            column("sname")
            column("fname", "forename")
            from("event_duration")
        }
        assertEquals(expected, select.asString(" "))
    }
    @Test
    public void testImplicitJoin() {
        String expected = "SELECT * FROM table AS t1, table AS t2 WHERE t1.id = t2.id;"

        Select select = new Select(new MySql())
        select.with {
            columns().column("*")
            from("table", "t1")
            from("table", "t2")
            where("t1.id").equal().literalValue("t2.id") // TODO add column
        }
        assertEquals(expected, select.asString(" "))
    }

    @Test
    public void testOrWhere() {
        Select select = new Select(new MySql())
        select.with {
            columns().column("colour")
            from("table")
            where("colour").equal().value("Red")
            orWhere("colour").equal().value("Blue")
        }
        assertEquals("SELECT colour FROM table WHERE colour = 'Red' OR colour = 'Blue';", select.asString(" "))
    }
    @Test
    public void testAndWhere() {
        Select select = new Select(new MySql())
        select.with {
            columns().column("colour")
            from("table")
            where("colour").equal().value("Red")
            andWhere("colour").equal().value("Blue")
        }
        assertEquals("SELECT colour FROM table WHERE colour = 'Red' AND colour = 'Blue';", select.asString(" "))
    }

    @Test
    public void testJoinMySql() {
        Select select = new Select(new MySql())
        select.with {
            columns().column("colour")
            from("table").setSchema("geiger")
            join("table2").on("id", "id2").setSchema("geiger")
            join("table3").on("id2", "id3").setSchema("geiger")
            where("colour").equal().value("red")
        }
        assertEquals("SELECT colour FROM geiger.table JOIN geiger.table2 ON id = id2 JOIN geiger.table3 ON id2 = id3 WHERE colour = 'red';", select.asString(" "))
    }
    @Test
    public void testJoinH2() {
        Select select = new Select(new H2())
        select.with {
            columns().column("colour")
            from("table")
            join("table2").on("id", "id2").setSchema("geiger")
            join("table3").on("id2", "id3").setSchema("geiger")
            where("colour").equal().value("red")
        }
        assertEquals("SELECT colour FROM table JOIN table2 ON id = id2 JOIN table3 ON id2 = id3 WHERE colour = 'red';", select.asString(" "))
    }

    @Test
    public void testGroupBy() {
        Select select = new Select(new MySql())
        select.with {
            columns().column("colour")
            from("table")
            groupBy("colour")
        }
        assertEquals("SELECT colour FROM table GROUP BY colour;", select.asString(" "))
    }

    @Test
    public void testGroupByList() {
        Select select = new Select(new MySql())
        List<String> columnList = ["colour", "size"]
        select.with {
            columns(columnList)
            from("table")
            groupBy(columnList as String[])
        }
        assertEquals("SELECT colour, size FROM table GROUP BY colour, size;", select.asString(" "))
    }

    @Test
    public void testClearGroupBy() {
        Select select = new Select(new MySql())
        List<String> columnList = ["colour", "size"]
        select.with {
            columns(columnList)
            from("table")
            groupBy(columnList as String[])
        }
        select.clearGroupBy()
        assertEquals("SELECT colour, size FROM table;", select.asString(" "))
    }

    @Test
    public void testComplexSelectWithNewlines() {
        Select subSelect = new Select(new ConvertFactory().create("MySql"))
        subSelect.with {
            columns().column("c.available_colour", "colour").column("m.material_type", "type").max("p.cost", "max_cost")
            from("colours", "c").setSchema("geiger")
            from("material", "m").setSchema("geiger")
            join("price", "p").on("p.id", "m.id").setSchema("geiger")
            where("c.available_colour").equal().value("red")
            andWhere("p.price").lessThan().value(20.99)
            groupBy("m.type").withoutSemiColon()
        }
        String subSelectString = "(${subSelect.asString()})"
        Select select = new Select(new MySql())
        select.columns().column("*")
        select.from(subSelectString, "sub")
        select.where("sub.rank").equal().value(21)

        String expected = """SELECT *
FROM (SELECT c.available_colour AS colour,
m.material_type AS type,
MAX(p.cost) AS max_cost
FROM geiger.colours AS c,
geiger.material AS m
JOIN geiger.price AS p ON p.id = m.id
WHERE c.available_colour = 'red'
AND p.price < 20.99
GROUP BY m.type) AS sub
WHERE sub.rank = 21;"""
        assertEquals(expected, select.asString())
    }
    @Test
    public void testClone() {
        Select select = new Select(new ConvertFactory().create("MySql"))
        select.with {
            columns().column("c.available_colour", "colour").column("m.material_type", "type").max("p.cost", "max_cost")
            from("colours", "c").setSchema("geiger")
            from("material", "m").setSchema("geiger")
            join("price", "p").on("p.id", "m.id").setSchema("geiger")
            where("c.available_colour").equal().value("red")
            andWhere("p.price").lessThan().value(20.99)
            groupBy("m.type")
        }

        String expectedClone = """SELECT c.available_colour AS colour,
m.material_type AS type,
MAX(p.cost) AS max_cost
FROM geiger.colours AS c,
geiger.material AS m
JOIN geiger.price AS p ON p.id = m.id
WHERE c.available_colour = 'red'
AND p.price < 20.99
GROUP BY m.type;"""
        String expectedChangedOriginal = """SELECT c.available_colour AS colour,
m.material_type AS type,
MAX(p.cost) AS max_cost
FROM geiger.colours AS c,
geiger.material AS m
JOIN geiger.price AS p ON p.id = m.id
WHERE c.available_colour = 'red'
AND p.price < 20.99
AND name = 'David'"""
        Select cloned = select.clone()

        select.with {
            clearGroupBy()
            withoutSemiColon()
            andWhere("name").equal().value("David")
        }
        assertEquals(expectedClone, cloned.asString())
        assertEquals(expectedChangedOriginal, select.asString())
    }
}
