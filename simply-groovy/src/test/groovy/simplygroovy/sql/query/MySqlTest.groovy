package simplygroovy.sql.query

import org.junit.Test
import static org.junit.Assert.assertEquals

/**
 * Created: 18/06/11
 * @author david
 */
class MySqlTest {
    @Test
    public void testFunction() {
        assertEquals("STR_TO_DATE", new MySql().function(Convert.FUNC_TO_DATE))
    }
    @Test
    public void testValue() {
        assertEquals("SYSDATE()", new MySql().value(Convert.VALUE_SYSDATE))
    }

    @Test
    public void testConvertDateFormat() {
//        "yyyy.MM.dd G 'at' HH:mm:ss z"	2001.07.04 AD at 12:08:56 PDT
        //        "EEE, MMM d, ''yy"	Wed, Jul 4, '01
        //        "h:mm a"	12:08 PM
        //        "hh 'o''clock' a, zzzz"	12 o'clock PM, Pacific Daylight Time
        //        "K:mm a, z"	0:08 PM, PDT
        //        "yyyyy.MMMMM.dd GGG hh:mm aaa"	02001.July.04 AD 12:08 PM
        //        "EEE, d MMM yyyy HH:mm:ss Z"	Wed, 4 Jul 2001 12:08:56 -0700
        //        "yyMMddHHmmssZ"	010704120856-0700

        checkConvertDateFormat("yyyy", "%Y")  // 4 digit year
        checkConvertDateFormat("yy", "%y")    // 2 digit year
        // Month
        checkConvertDateFormat("MMMMM", "%M") // January..December
        checkConvertDateFormat("MMMM", "%b")  // Jan..Dec
        checkConvertDateFormat("MM", "%m")    // 01-12 (actually 00-12 in MySql)
        checkConvertDateFormat("M", "%c")     // 1-12 (actually 0-12 in MySql)
        // Day
        checkConvertDateFormat("dd", "%d") // 01-31 (actually 00-31 in MySql)
        checkConvertDateFormat("d", "%e")  // 1-31  (actually 0-31 in MySql)
        // Week Day
        checkConvertDateFormat("EEEE", "%W") // Sunday..Saturday
        checkConvertDateFormat("EEE", "%a") // Sun..Sat
        // Hour
        checkConvertDateFormat("HH", "%H") // 00-23
        checkConvertDateFormat("H", "%k")  // 0-23
        checkConvertDateFormat("hh", "%h") // 01-12
        checkConvertDateFormat("h", "%l")  // 1-12
        // Minute
        checkConvertDateFormat("mm", "%i") // 00-59
        // Second
        checkConvertDateFormat("ss", "%s") // 00-59
        // Milli/Fractional
        checkConvertDateFormat("SSSSSS", "%f") // Java 000-999, MySQL 000000..999999
        // am/pm
        checkConvertDateFormat("a", "%p") // AM/PM
        // try them out together
        checkConvertDateFormat("yyyy-MM-dd HH:mm:ss", "%Y-%m-%d %H:%i:%s")
        checkConvertDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS", "%Y-%m-%d %H:%i:%s.%f")
        checkConvertDateFormat("yyyyMMMMM", "%Y%M")
        checkConvertDateFormat("h:mm:ssa", "%l:%i:%s%p")
    }

    private def checkConvertDateFormat(String javaFormat, String expected) {
        MySql convert = new MySql()
        String msg = "converting java format $javaFormat to expected MySql format $expected"
        assertEquals(msg, expected, convert.convertDateFormat(javaFormat))
    }
}
