package simplygroovy.sql

import groovy.sql.Sql

/**
 * Created: 29/03/11
 * @author david
 */
class ConnectionUtil {
    public static Sql createConnection(String env, File credentialsFile) {
        ConfigObject credentialsConfig = new ConfigSlurper(env).parse(credentialsFile.toURL())

        String sqlUsername = credentialsConfig.sql.username
        String sqlPassword = credentialsConfig.sql.password
        String driver = credentialsConfig.sql.driver
        String connectionString = credentialsConfig.sql.connectionstring
        Sql sql = Sql.newInstance(connectionString, sqlUsername, sqlPassword, driver)
        return sql
    }

}
