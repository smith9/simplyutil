package simplygroovy.sql

import groovy.sql.Sql
import org.junit.Test
import static junit.framework.Assert.assertEquals
import org.junit.Before
import org.apache.commons.io.FileUtils

/**
 * Created: 25/03/11
 * @author david
 */
class DefaultSqlToConfluenceTest {
    File testDir

    @Before
    void setup() {
        testDir = new File("DefaultSqlToConfluenceTest")
        FileUtils.deleteDirectory(testDir)
        testDir.mkdirs()
    }


    @Test
    public void testToConfluenceNoReplacements() {
        DefaultSqlToConfluence runner = new DefaultSqlToConfluence()

        Sql sql = createTestConnection()
        createTestTable(sql)
        final String queryString = "select * from test;"
        String table = runner.toTable(sql, queryString, [:])
        assertEquals("""||ID||FORENAME||SURNAME||DESCRIPTION||
|1|David|Smith||
|2|Joe|Blogs|code monkey|
|3|Albert|Einstein|Scientist|
""", table)
    }

    @Test
    public void testProcess() {
        File inputDir = new File(testDir, "sql")
        inputDir.mkdirs()
        File queryHeadingFile = new File(inputDir, "test1.prefix.txt")
        queryHeadingFile.text = """h3. Query with description
The following includes the description column"""
        File queryFile = new File(inputDir, "test1.sql")
        queryFile.text = "select * from test;"
        File queryHeadingFile2 = new File(inputDir, "test2.prefix.txt")
        queryHeadingFile2.text = """h3. Query without description
The following does not include the description column"""
        File queryFile2 = new File(inputDir, "test2.sql")
        queryFile2.text = 'select ${COLUMNS} from test;'
        final LinkedHashMap replacements = [COLUMNS:"id,forename,surname"]

        DefaultSqlToConfluence runner = new DefaultSqlToConfluence()

        Sql sql = createTestConnection()
        createTestTable(sql)
        String output = runner.processFiles(inputDir, sql, replacements)
        assertEquals("""h3. Query with description
The following includes the description column
||ID||FORENAME||SURNAME||DESCRIPTION||
|1|David|Smith||
|2|Joe|Blogs|code monkey|
|3|Albert|Einstein|Scientist|
h3. Query without description
The following does not include the description column
||ID||FORENAME||SURNAME||
|1|David|Smith|
|2|Joe|Blogs|
|3|Albert|Einstein|
""", output)
    }

    def createTestTable(Sql sql) {
        File testData = new File(getClass().getClassLoader().getResource('sql/input/database.csv').toURI())
        createTableFromCsv(sql, "test", testData)
    }

    private Sql createTestConnection() {
        String sqlusername = "sa"
        String sqlpassword = ""
        String driver = "org.h2.Driver"
        String connectionstring = "jdbc:h2:temp"

        Sql sql = Sql.newInstance(connectionstring, sqlusername, sqlpassword, driver)
        return sql
    }

    public void createTableFromCsv(Sql sql, String tableName, File csv) {
        // FIX - when run on a new machine, need to avoid doing drop when table does not exist
        sql.execute("drop table IF EXISTS ${Sql.expand tableName};")
        sql.execute("create table ${Sql.expand tableName} as select * from CSVREAD('${Sql.expand csv.absolutePath}');")
    }

//    public void createDataBase(List<String> header) {
//        String sqlusername = "sa"
//        String sqlpassword = ""
//        String driver = "org.h2.Driver"
//        String connectionstring = "jdbc:h2:~/temp"
//
//        Sql sql = Sql.newInstance(connectionstring, sqlusername, sqlpassword, driver)
//        sql.execute("create table sqltest(id int primary key, firstname varchar, lastname varchar);")
//        sql.eachRow("show tables;") {
//            println it
//        }
//    }
//    private void xxx() {
//        ResultSet rs = Csv.getInstance().
//            read("data/test.csv", null, null);
//        ResultSetMetaData meta = rs.getMetaData();
//        while (rs.next()) {
//            for (int i = 0; i < meta.getColumnCount(); i++) {
//                System.out.println(
//                    meta.getColumnLabel(i + 1) + ": " +
//                    rs.getString(i + 1));
//            }
//            System.out.println();
//        }
//        rs.close();
//    }
}
