package simplygroovy.util

import org.joda.time.DateTime
import org.junit.Test
import static junit.framework.Assert.assertEquals
import simplygroovy.bean.Period

/***
 * Created: 28/03/11
 * @author david
 */
class DateUtilTest {
    @Test
    public void testAsFilenameString() {
        final DateTime dateTime = new DateTime(2011, 03, 28, 17, 20, 0, 0)
        assertEquals("2011-03-28_17_20_00", DateUtil.asFilenameString(dateTime))
    }
    @Test
    public void testLevel() {
        final DateTime dateTime = new DateTime(2011, 03, 28, 23, 59, 59, 999)
        assertEquals(new DateTime(2011, 03, 28, 23, 59, 0, 0), DateUtil.level(dateTime, Period.MINUTE))
        assertEquals(new DateTime(2011, 03, 28, 23, 0, 0, 0), DateUtil.level(dateTime, Period.HOUR))
        assertEquals(new DateTime(2011, 03, 28, 0, 0, 0, 0), DateUtil.level(dateTime, Period.DAY))
        assertEquals(new DateTime(2011, 03, 1, 0, 0, 0, 0), DateUtil.level(dateTime, Period.MONTH))
        assertEquals(new DateTime(2011, 1, 1, 0, 0, 0, 0), DateUtil.level(dateTime, Period.YEAR))
    }
}
