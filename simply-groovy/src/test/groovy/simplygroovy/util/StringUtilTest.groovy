package simplygroovy.util

import org.junit.Test
import static org.junit.Assert.assertEquals

/**
 * Created: 18/06/11
 * @author david
 */
class StringUtilTest {
    @Test
    public void testSplitSame() {
        checkSplitSame("MMMMYYYY:DD:SSSSSS", ["MMMM", "YYYY", ":", "DD", ":", "SSSSSS"])
        checkSplitSame("MMMM", ["MMMM"])
        checkSplitSame("", [])
    }

    void checkSplitSame(String input, List<String> expected) {
        List<String> actual = new StringUtil().splitSame(input)
        assertEquals("splitting $input", expected, actual)
    }
}
