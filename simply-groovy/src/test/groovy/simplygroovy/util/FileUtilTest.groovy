package simplygroovy.util;


import org.junit.Test
import org.apache.commons.io.FileUtils
import org.junit.Before
import static junit.framework.Assert.*

/**
 * User: david
 * Date: 16/11/12
 * Time: 9:50 PM
 */
public class FileUtilTest {
    File testDir
    @Before
    public void setup() {
        testDir = new File(org.apache.commons.io.FileUtils.getTempDirectory(), "FileUtilsTest")
        testDir.mkdirs()
        org.apache.commons.io.FileUtils.cleanDirectory(testDir)
        org.apache.commons.io.FileUtils.forceDeleteOnExit(testDir)

    }
    @Test
    public void testZipUnzip() throws Exception {
        File inputFile = new File(testDir, "input.txt")
        inputFile.text = "hello"
        File zipFile = FileUtil.zipFile(inputFile, true)
        assertTrue(zipFile.exists())
        assertFalse(inputFile.exists())
        List<File> unzippedFiles = FileUtil.unzip(zipFile, testDir)
        assertEquals(1, unzippedFiles.size())
        assertEquals(new File(testDir, "input.txt").absolutePath, unzippedFiles.first().absolutePath)
        assertEquals("hello", new File(testDir, "input.txt").text)
    }
}
