package simplygroovy.util

import org.junit.Test
import static org.junit.Assert.assertEquals

/**
 * Created: 21/12/11
 * @author david
 */
class TimeThisTest {

    @Test
    public void testTime() {
        long milli = TimeThis.time {
            sleep(100)
        }
        org.junit.Assert.assertEquals(100, milli, 10)
    }
    @Test
    public void testHuman() {
        assertEquals("0 milli", TimeThis.human(0))
        assertEquals("999 milli", TimeThis.human(TimeThis.SECOND-1))
        assertEquals("1.0s", TimeThis.human(TimeThis.SECOND))
        assertEquals("1.001s", TimeThis.human(TimeThis.SECOND+1))
        assertEquals("59.999s", TimeThis.human(TimeThis.MINUTE-1))
        assertEquals("1m", TimeThis.human(TimeThis.MINUTE))
        assertEquals("1m 0.001s", TimeThis.human(TimeThis.MINUTE+1))
        assertEquals("1m 1.0s", TimeThis.human(TimeThis.MINUTE+TimeThis.SECOND))
        assertEquals("59m 59.999s", TimeThis.human(TimeThis.HOUR-1))
        assertEquals("1h", TimeThis.human(TimeThis.HOUR))
        assertEquals("1h 0m 0.001s", TimeThis.human(TimeThis.HOUR+1))
        assertEquals("1h 1m 1.0s", TimeThis.human(TimeThis.HOUR+TimeThis.MINUTE+TimeThis.SECOND))
        assertEquals("1h 59m 59.999s", TimeThis.human(2*TimeThis.HOUR-1))
    }
}
