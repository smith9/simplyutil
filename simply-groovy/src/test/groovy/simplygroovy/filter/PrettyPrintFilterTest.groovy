package simplygroovy.filter;


import org.junit.Test
import org.apache.commons.io.IOUtils
import static org.junit.Assert.assertEquals

/**
 * User: david
 * Date: 15/11/12
 * Time: 9:48 PM
 */
public class PrettyPrintFilterTest {
    @Test
    public void testFilter() throws Exception {
        String inputString = """<?xml version="1.0" encoding="UTF-8"?><root><name first="david" last="smith"></name>
  <colour>Red</colour>  <a>   <b>banana boat</b>  </a></root>"""

        String expected = """<?xml version="1.0" encoding="UTF-8"?>
<root>
   <name last="smith" first="david"></name>
   <colour>Red</colour>
   <a>
      <b>banana boat</b>
   </a>
</root>"""

        PrettyPrintFilter prettyPrinter = new PrettyPrintFilter();
        InputStream result = prettyPrinter.filterStream(IOUtils.toInputStream(inputString, "UTF-8"))
        assertEquals(expected, IOUtils.toString(result))

    }
}
