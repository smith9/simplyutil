package simplygroovy.filter

import org.junit.Test
import org.apache.commons.io.IOUtils
import static org.junit.Assert.assertEquals

/**
 * User: david
 * Date: 15/11/12
 * Time: 8:58 PM
 */
class GrepFilterTest {
    @Test
    public void testFilterNoName() throws Exception {
        StreamFilter filter = new GrepFilter("David")
        filter.printInputName = false
        String input = """This is some input
with multiple lines
some contain David
and some do not
there are two Davids in total"""
        ByteArrayOutputStream output = new ByteArrayOutputStream()
        InputStream result =filter.filterStream(IOUtils.toInputStream(input, "UTF-8"))
        assertEquals("""some contain David
there are two Davids in total
""", IOUtils.toString(result))
    }
    @Test
    public void testFilterName() throws Exception {
        StreamFilter filter = new GrepFilter("David")
        filter.inputName = "name"
        String input = """This is some input
with multiple lines
some contain David
and some do not
there are two Davids in total"""
        ByteArrayOutputStream output = new ByteArrayOutputStream()
        InputStream result = filter.filterStream(IOUtils.toInputStream(input, "UTF-8"))

        assertEquals("""name(3): some contain David
name(5): there are two Davids in total
""", IOUtils.toString(result))
    }
    @Test(expected=IllegalStateException)
    public void testFilterEmptyName() throws Exception {
        StreamFilter filter = new GrepFilter("David")
        String input = """This is some input
with multiple lines
some contain David
and some do not
there are two Davids in total"""
        ByteArrayOutputStream output = new ByteArrayOutputStream()
        filter.filterStream(IOUtils.toInputStream(input, "UTF-8"))
    }
}
