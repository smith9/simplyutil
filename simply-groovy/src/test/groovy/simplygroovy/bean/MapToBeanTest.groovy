package simplygroovy.bean

import java.text.SimpleDateFormat
import org.junit.Test
import static junit.framework.Assert.assertEquals
import static junit.framework.Assert.assertNull

/**
 * Created: Mar 16, 2011
 * @author david
 */
class MapToBeanTest {
    @Test(expected=RuntimeException)
    public void testRegisterEnumConvert() {
        MapToBean.registerEnumClass(Bean)
    }
    @Test
    public void testConvert() {
        Map properties = [
            myEnum: "FIRST",
            dateTimeValue: "2011-03-16 20:42:55",
            integerValue: "21",
            dateValue: "18/04/2011",
            intValue: "18",
            stringValue: "some string",
            doubleValue: "20.55555555555555"]
        MapToBean.registerEnumClass(MyEnum)
        MapToBean.setJodaDateTimeFormat ("yyyy-MM-dd HH:mm:ss")
        MapToBean.setDateFormat ("dd/MM/yyyy")
        Bean bean = MapToBean.convert(Bean.class, properties)
        assertEquals(MyEnum.FIRST, bean.myEnum)
        assertEquals(20.55555555555555d, bean.doubleValue)
        assertEquals(21, bean.integerValue)
        assertEquals(18, bean.intValue)
        assertEquals("some string", bean.stringValue)
        assertEquals("2011-03-16 20:42:55", bean.dateTimeValue.toString("yyyy-MM-dd HH:mm:ss"))
        assertEquals("18/04/2011", toString(bean.dateValue))
    }
    @Test
    public void testConvertLessPropertiesThanBean() {
        Map properties = [
            dateTimeValue: "2011-03-16 20:42:55",
            stringValue: "some string",
            integerValue:null]

        MapToBean.setJodaDateTimeFormat ("yyyy-MM-dd HH:mm:ss")
        MapToBean.setDateFormat ("dd/MM/yyyy")
        Bean bean = MapToBean.convert(Bean.class, properties)
        assertEquals(0d, bean.doubleValue)
        assertNull(bean.integerValue)
        assertEquals(0, bean.intValue)
        assertEquals("some string", bean.stringValue)
        assertEquals("2011-03-16 20:42:55", bean.dateTimeValue.toString("yyyy-MM-dd HH:mm:ss"))
        assertNull(bean.dateValue)
    }

    private static String toString(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy")
        return format.format(date)
    }

}
