package simplygroovy.bean;


import org.junit.Test
import static org.junit.Assert.assertTrue
import static org.junit.Assert.assertFalse

/**
 * Created: 6/06/12
 *
 * @author david
 */
public class PeriodTest {
    @Test
    public void testIsGreaterMinute() throws Exception {
        [Period.MINUTE, Period.HOUR, Period.DAY, Period.MONTH, Period.YEAR].each { Period period ->
            assertFalse(Period.MINUTE.isGreater(period))
        }
    }
    @Test
    public void testIsGreaterHour() throws Exception {
        checkGreaterThan([Period.MINUTE], Period.HOUR, [Period.HOUR, Period.DAY, Period.MONTH, Period.YEAR])
    }
    @Test
    public void testIsGreaterDay() throws Exception {
        checkGreaterThan([Period.MINUTE, Period.HOUR], Period.DAY, [Period.DAY, Period.MONTH, Period.YEAR])
    }
    @Test
    public void testIsGreaterMonth() throws Exception {
        checkGreaterThan([Period.MINUTE, Period.HOUR, Period.DAY], Period.MONTH, [Period.MONTH, Period.YEAR])
    }

    @Test
    public void testIsGreaterYear() throws Exception {
        checkGreaterThan([Period.MINUTE, Period.HOUR, Period.DAY, Period.MONTH], Period.YEAR, [Period.YEAR])
    }

    private def checkGreaterThan(ArrayList<Period> lesserPeriods, Period checkPeriod, ArrayList<Period> greaterOrEqualPeriods) {
        greaterOrEqualPeriods.each { Period period ->
            assertFalse(checkPeriod.isGreater(period))
        }
        lesserPeriods.each { Period lesserPeriod ->
            assertTrue(checkPeriod.isGreater(lesserPeriod))
        }
    }
}
