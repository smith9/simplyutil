package simplygroovy.bean

import org.joda.time.DateTime

/**
 * Created: Mar 16, 2011
 * @author david
 */
class Bean {
    MyEnum myEnum
    DateTime dateTimeValue
    Integer integerValue
    Date dateValue
    int intValue
    String stringValue
    double doubleValue
}
