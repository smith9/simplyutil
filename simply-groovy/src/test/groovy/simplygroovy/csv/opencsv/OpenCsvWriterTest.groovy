package simplygroovy.csv.opencsv

import simplygroovy.csv.CsvWriter

import org.apache.commons.io.FileUtils
import org.junit.Before
import org.junit.Test
import static junit.framework.Assert.assertEquals

/**
 * Created: 24/03/11
 * @author david
 */
class OpenCsvWriterTest {
    File outputDir
    @Before
    public void setup() {
        outputDir = new File("output")
        FileUtils.deleteDirectory(outputDir)
        outputDir.mkdirs()

    }
    @Test
    public void testWriteEmpty() {
        File outputCsv = new File(outputDir, "output.csv")
        CsvWriter writer = new OpenCsvWriter(outputCsv)
        writer.close()
        assertEquals("", outputCsv.text)
    }

    @Test
    public void testWriteHeader() {
        String expected =
        """"h1","h2","h,3 ","h4","h5"
"""
        File outputCsv = new File(outputDir, "output.csv")

        CsvWriter writer = new OpenCsvWriter(outputCsv)
        writer.writeRow(["h1", "h2", "h,3 ", "h4", "h5"])
        writer.close()
        assertEquals(expected, outputCsv.text)
    }

    @Test
    public void testWriteHeaderAndRows() {
        String expected =
        """"h1","h2","h,3 ","h4","h5"
"a1","a2 2","a3","a,4","a5"
"b1","b2 2",,"b,4","b5"
"""
        File outputCsv = new File(outputDir, "output.csv")
        CsvWriter writer = new OpenCsvWriter(outputCsv)
        writer.writeRow(["h1", "h2", "h,3 ", "h4", "h5"])
        writer.writeRow(["a1", "a2 2", "a3", "a,4", "a5"])
        writer.writeRow(["b1", "b2 2", null, "b,4", "b5"])
        writer.close()
        assertEquals(expected, outputCsv.text)
    }
}
