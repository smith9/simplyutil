package simplygroovy.csv.opencsv

import org.junit.Test
import static junit.framework.Assert.assertEquals
import static junit.framework.Assert.assertNull
import simplygroovy.csv.Header
import simplygroovy.csv.Row
import static org.junit.Assert.assertTrue
import static org.junit.Assert.assertFalse

/**
 * Created: Mar 15, 2011
 * @author david
 */
class OpenCsvReaderTest {
    @Test
    void testReaderConstructor() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/input.csv').toURI())
        Reader fileReader = new FileReader(input)
        OpenCsvReader reader = new OpenCsvReader(fileReader)
        reader.readNext()
        List expected = ["a1", "a2 2", "a3", "a,4", "a5"]
        List actual = reader.getHeader().collect { reader.getCurrentValue(it)}
        assertEquals(expected, actual)
    }

    @Test
    void testHeader() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/input.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        List<String> header = reader.getHeader()
        junit.framework.Assert.assertEquals(["h1", "h2", "h,3 ", "h4", "h5"], header)
    }
    
    @Test
    void testGetCurrentValue() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/input.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        reader.readNext()
        List expected = ["a1", "a2 2", "a3", "a,4", "a5"]
        List actual = reader.getHeader().collect { reader.getCurrentValue(it)}
        assertEquals(expected, actual)
    }

    @Test (expected=IllegalArgumentException.class)
    void testGetCurrentValueBadColumn() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/input.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        reader.readNext()
        reader.getCurrentValue("not a column in the header")
    }

    @Test
    void testRowsAsMap() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/input.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        ["h1", "h2", "h,3 ", "h4", "h5"]
        assertEquals([h1:"a1",h2:"a2 2","h,3 ":"a3",h4:"a,4",h5:"a5"], reader.readNextAsMap())
        assertEquals([h1:"b1",h2:"b2","h,3 ":"b3",h4:"b,4",h5:"b5"], reader.readNextAsMap())
        2.times {
            assertNull(reader.readNext())
        }

    }

    @Test
    void testEmptyAsMap() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/inputEmpty.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        assertNull(reader.getHeader())
        2.times {
            assertNull(reader.readNextAsMap())
        }
    }
    @Test
    void testOnlyHeaderAsMap() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/inputOnlyHeader.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        assertEquals(["h1", "h2", "h,3 ", "h4", "h5"], reader.getHeader())
        2.times {
            assertNull(reader.readNextAsMap())
        }
    }

    @Test
    void testRows() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/input.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        assertEquals(["a1","a2 2","a3","a,4","a5"], reader.readNext())
        assertEquals(["b1","b2","b3","b,4","b5"], reader.readNext())
        2.times {
            assertNull(reader.readNext())
        }

    }
    @Test
    void testEachRowMap() {
//        h1,h2,"h,3 ",h4,"h5"
//a1,a2 2,a3,"a,4",a5
//b1,b2,b3,"b,4",b5
        File input = new File(getClass().getClassLoader().getResource('csv/input/input.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        Header header
        Row row1
        Row row2
        int numRows = 0
        reader.eachRowMapWithIndex { Header csvHeader, Row row, int index ->
            if(!header) {
                header = csvHeader
            }
            if(index == 0) {
                row1 = row
            }
            if(index == 1) {
                row2 = row
            }
            numRows++
        }
        assertEquals(["a1","a2 2","a3","a,4","a5"], row1.values)
        assertEquals(["b1","b2","b3","b,4","b5"], row2.values)
        assertEquals(["h1","h2","h,3 ","h4","h5"], header.columns)
        assertEquals("a3", row1.getValue("h,3 "))
        assertEquals("a,4", row1.getValue("h4"))
        assertEquals("b3", row2.getValue("h,3 "))
        assertEquals("b,4", row2.getValue("h4"))
        assertEquals(2, numRows)
        assertTrue(header.containsColumn("h,3 "))
        assertFalse(header.containsColumn("h,3"))
    }
    @Test
    void testEmpty() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/inputEmpty.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        assertNull(reader.getHeader())
        2.times {
            assertNull(reader.readNext())
        }
    }
    @Test
    void testOnlyHeader() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/inputOnlyHeader.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        assertEquals(["h1", "h2", "h,3 ", "h4", "h5"], reader.getHeader())
        2.times {
            assertNull(reader.readNext())
        }
    }

}
